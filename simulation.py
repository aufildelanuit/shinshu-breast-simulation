"""
Functions and methods to be used with FEniCS.
Created for the needs of a research topic on breast deformation.
OPOLKA Yohann, Shinshu University, Koseki Lab., 2020.02.10
Latest revision: 2020.07.10
"""

# for most operations on finite elements and for the main calculation
import fenics
# fenics and dolfin are supposed to have the same contents
# keeping this line though in case one of the two were to be deprecated later
# #import dolfin
# for reading and manipulating the mesh
import meshio
# for plotting in 2D and/or 3D (not needed if plots deactivated)
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# for numerical operations, etc.
import numpy
import ufl
from collections import namedtuple
from mpi4py import MPI
# mostly ? used for debugging
import sys
from petsc4py import PETSc


loadweight_default = 0.000
loadweight_offset_default = 0.030  # 0.000
quarter_mesh_default = False
log_level_default = 30
plot_level_default = 0
use_vacuum_default = False
solver_default = "linear"
sensors_offset_angle_default = 0
sensors_offset_height_default = 0

rho_values_default = [1092, 970, 970]  # [1092, 970, 970]
young_modulus_values_default = [95359.25, 2075.28, 2075.28]  # [66053.125, 1437.500, 1437.500]
poisson_ratio_values_default = [0.49, 0.47, 0.47]  # [0.49, 0.47, 0.47]

stability_criterion = 0.0001  # no unit, relative


def breakpoint():
    #  ####  #For debugging
    print(f"{mpi_rank:d} : ok")
    # force printing to stdout
    sys.stdout.flush()
    exit(0)
    #####


# To be set outside of the class as it is not instance-specific ??
# Use the Web Agent for quick visualization in the web browser with matplotlib
def initialize_matplotlib(plot_level, log_level):
    if plot_level > 0:
        if log_level < 30:
            print(">> Initializing matplotlib...")
        mpl.use('WebAgg')


# Define strain and stress functions for computation
def epsilon(u):
    return 0.5*(fenics.nabla_grad(u) + fenics.nabla_grad(u).T)


def sigma(u, lambda_, mu, d):
    return lambda_*ufl.nabla_div(u)*fenics.Identity(d) + 2*mu*epsilon(u)


# Function previously used for estimating the surface area on which the load is applied
# Probably not needed anymore, but kept for reference
def ellipse_area(a, b):
    return ((numpy.pi)*a*b)


# Function returning the 3 components (x, y, z) of the cross product of two vectors
# The two vectors should be given as arrays of their 3D coordinates (x, y, z)
def cross_product(a, b):
    return numpy.asarray([(a[1]*b[2] - a[2]*b[1]), (a[0]*b[2] - a[2]*b[0]), (a[0]*b[1] - a[1]*b[0])])


# Function returning the norm of a 3D vector given by its (x, y, z) coordinates
def vector_norm(v):
    v = numpy.asarray(v)
    if len(v.shape) == 1:
        return numpy.sqrt((v[0])**2 + (v[1])**2 + (v[2])**2)
    if len(v.shape) == 2:
        if ((numpy.asarray(v.shape) == 3).nonzero()[0][0]) == 0:
            return numpy.sqrt((v[0, :])**2 + (v[1, :])**2 + (v[2, :])**2)
        if ((numpy.asarray(v.shape) == 3).nonzero()[0][0]) == 1:
            return numpy.sqrt((v[:, 0])**2 + (v[:, 1])**2 + (v[:, 2])**2)


# Function used for calculating the surface area of each facet
# from the coordinates of its points (as arrays)
def triangle_area(triangle):
    # name the 3 points for readability, and associate them diractly with arrays containing their coordinates
    A = triangle[0]
    B = triangle[1]
    C = triangle[2]
    x, y, z = cross_product((B-A), (C-A))
    # returns 1/2 * || AB x AC ||  , AB and AC being vectors, x denoting a cross product, and || representing the norm of the resulting vector
    return (1./2 * vector_norm([x, y, z]))


# a vector namedtuple to easily store and access force components
# not to be mistaken with the ".vector()" method inherited by some ufl (and FEniCS) objects
vector = namedtuple('vector', ['x', 'y', 'z'])


# a function used for searching an optimum (min or max) by 3-point dichotomy
# uses left or right guesses to optimize the research time
def search_by_dichotomy(
    function,
    left_limit,
    right_limit,
    max_iterations=50,
    data_dictionary=None,
    search_tolerance=0.00001,
    stability_criterion=stability_criterion,
    log_level=25,
    log_level_print_limit=35,
    search_for="min"
):
    next_step = 'L'
    stop = False
    ms_iter = 0
    progress = 0
    # check that the left and right limit are not inverted
    if right_limit < left_limit:
        temp_var = right_limit
        right_limit = left_limit
        left_limit = temp_var
    if data_dictionary is not None:
        data_dictionary.update({
            "current_left_limit": left_limit,
            "current_right_limit": right_limit
        })
    while True:
        ms_iter += 1
        # preparation
        mid = (left_limit + right_limit)/2
        if next_step == 'R':
            quarter = (mid + right_limit)/2
        elif next_step == 'L':
            quarter = (left_limit + mid)/2
        # actual calculation
        if log_level < log_level_print_limit:
            #print(data_dictionary)
            print(f"\r>># Searching optimum within [{left_limit:f}, {right_limit:f}], iteration {ms_iter:d}  \t~ {int(numpy.round(progress)):d}%  \t", end='')
            sys.stdout.flush()
        f_quarter = function(quarter)
        # save data
        if data_dictionary is not None:
            if quarter not in data_dictionary:
                data_dictionary.update({quarter: {}})
            data_dictionary[quarter].update({
                "output_f": f_quarter
            })
        f_mid = function(mid)
        # save data
        if data_dictionary is not None:
            if mid not in data_dictionary:
                data_dictionary.update({mid: {}})
            data_dictionary[mid].update({
                "output_f": f_mid
            })
        if numpy.abs(f_mid) < stability_criterion:
            f_mid_divsafe = stability_criterion
        else:
            f_mid_divsafe = f_mid
        # progress
        print(f_mid_divsafe, stability_criterion)
        progress = numpy.log10(numpy.abs((f_mid_divsafe - f_quarter)/f_mid_divsafe))/(numpy.log10(stability_criterion))*100
        if progress < 0:
            progress = 0
        if progress > 100:
            progress = 100
        # test : optimum found? search search_tolerance
        if numpy.abs(right_limit - left_limit) < search_tolerance:
            stop = True
        # test : optimum found? stability_criterion
        if (numpy.abs((f_mid - f_quarter)/f_mid_divsafe) < stability_criterion) and (stop == False):
            f_check = function((quarter + mid)/2)
            # save data
            if data_dictionary is not None:
                if quarter not in data_dictionary:
                    data_dictionary.update({((quarter + mid)/2): {}})
                data_dictionary[((quarter + mid)/2)].update({
                    "output_f": f_check
                })
            if numpy.abs((f_mid - f_check)/f_mid_divsafe) < stability_criterion:
                stop = True
        # preparing for the next iteration
        if not stop:
            current_step = next_step
            if search_for == "min":
                if f_quarter >= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = quarter
                        next_step = 'R'
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = quarter
                        next_step = 'L'
                if f_quarter <= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = mid
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = mid
            elif search_for == "max":
                if f_quarter <= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = quarter
                        next_step = 'R'
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = quarter
                        next_step = 'L'
                if f_quarter >= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = mid
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = mid
        # feedback
        if log_level < (log_level_print_limit-10):
            print(f"f = {f_mid:f}, next:{next_step:s}, stop:{stop:b}")
        # loop break conditions
        if ms_iter >= max_iterations:
            break
        if stop:
            break
        # End of the search loop
    if log_level < log_level_print_limit:
        print("")
    return (mid, f_mid, ms_iter)


# convert degree angles to rad
def degree_to_rad(angle_degree):
    return (angle_degree * numpy.pi / 180)


# convert rad angles to degree
def rad_to_degree(angle_rad):
    return (angle_rad * 180 / numpy.pi)


# solver: linear model
def solve_with_linear_model(simulation, comm=fenics.MPI.comm_world):
    # The problem is linearized in the form: a(u, v) = L(v)
    # "a" collects all the unknown functions (u, here)
    # "L" should normally be known (function of v only)
    # a = Sum_i,j(sigma(u)_i,j ・ epsilon(v)_i,j) * dx = (sigma(u):epsilon(v))dx in mathematical notation
    # this operation is the "inner product" defined in fenics.inner
    a = fenics.inner(sigma(u=simulation.u, lambda_=simulation.lambda_, mu=simulation.mu, d=3), epsilon(simulation.v))*fenics.dx
    L = fenics.dot(ufl.as_vector(simulation.fv), simulation.v)*fenics.dx + fenics.dot(ufl.as_vector(simulation.Ps), simulation.v)*fenics.ds
    # And solve the problem
    # reminder: bc is the set of boundary conditions defined earlier
    A = fenics.assemble(a)
    b = fenics.assemble(L)
    if numpy.asarray(simulation.bc).size > 1:
        for bc_i in simulation.bc:
            bc_i.apply(A, b)
    else:
        simulation.bc.apply(A, b)
    #solver = fenics.PETScLUSolver(comm, "mumps")
    solver = fenics.PETScKrylovSolver("default", "hypre_parasails")  # "petsc_amg"
    prm = solver.parameters  # short form
    prm["absolute_tolerance"] = 1E-10
    prm["relative_tolerance"] = 1E-8
    prm["maximum_iterations"] = 10000
    if simulation.log_level < 16:
        prm["monitor_convergence"] = True
    prm["divergence_limit"] = 10e1
    prm["nonzero_initial_guess"] = True

    #solver.parameters["verbose"] = True
    solver.solve(A, simulation.u_computed.vector(), b)
    #fenics.solve(a == L, simulation.u_computed, simulation.bc, solver_parameters={'linear_solver': 'mumps'})
    #fenics.solve(a == L, simulation.u_computed, simulation.bc)


def solve_with_neo_hookean_model(simulation):
    # Kinematics
    dimension = len(simulation.u_computed)
    # Identity tensor
    identity_tensor = fenics.Identity(dimension)
    # Deformation gradient
    F = identity_tensor + fenics.grad(simulation.u_computed)
    # Right Cauchy-Green tensor
    C = F.T*F
    # Invariants of deformation tensors
    Ic = fenics.tr(C)
    J = fenics.det(F)
    # Stored strain energy density (incompressible neo-Hookean model)
    psi = (1/2*simulation.mu)*(Ic - 3) - simulation.mu*fenics.ln(J) + (simulation.lambda_/2)*(fenics.ln(J))**2
    # Total potential energy
    Pi = psi*fenics.dx - fenics.dot(ufl.as_vector(simulation.fv), simulation.u_computed)*fenics.dx - fenics.dot(ufl.as_vector(simulation.Ps), simulation.u_computed)*fenics.ds
    # Compute first variation of Pi (directional derivative about u in the direction of v)
    F = fenics.derivative(Pi, simulation.u_computed, simulation.v)
    # Compute Jacobian of F
    J = fenics.derivative(F, simulation.u_computed, simulation.u)

    problem = fenics.NonlinearVariationalProblem(F, simulation.u_computed, simulation.bc, J)
    solver = fenics.NonlinearVariationalSolver(problem)
    prm = solver.parameters["newton_solver"]
    prm["absolute_tolerance"] = 1E-8
    prm["relative_tolerance"] = 1E-7
    prm["maximum_iterations"] = 50
    prm["relaxation_parameter"] = 0.65
    prm["linear_solver"] = "bicgstab"
    prm["preconditioner"] = "hypre_parasails"

    # Solve variational problem
    solver.solve()
    #fenics.solve(F == 0, simulation.u_computed, simulation.bc, J=J, solver_parameters={"newton_solver": {"relative_tolerance": 1.0e-5, "convergence_criterion": "incremental"}})


def solve_with_yeoh_model(simulation):
    # Kinematics
    dimension = len(simulation.u_computed)
    # Identity tensor
    identity_tensor = fenics.Identity(dimension)
    # Deformation gradient
    F = identity_tensor + fenics.grad(simulation.u_computed)
    # Right Cauchy-Green tensor
    C = F.T*F
    # Invariants of deformation tensors
    Ic = fenics.tr(C)
    J = fenics.det(F)
    # Stored strain energy density (incompressible neo-Hookean model)
    psi = (simulation.mu/2)*(Ic - 3) + (simulation.mu/2)*((Ic - 3)**2) + (simulation.mu/2)*((Ic - 3)**3)
    # Total potential energy
    Pi = psi*fenics.dx - fenics.dot(ufl.as_vector(simulation.fv), simulation.u_computed)*fenics.dx - fenics.dot(ufl.as_vector(simulation.Ps), simulation.u_computed)*fenics.ds
    # Compute first variation of Pi (directional derivative about u in the direction of v)
    F = fenics.derivative(Pi, simulation.u_computed, simulation.v)
    # Compute Jacobian of F
    J = fenics.derivative(F, simulation.u_computed, simulation.u)
    # Solve variational problem
    fenics.solve(F == 0, simulation.u_computed, simulation.bc, J=J, solver_parameters={'nonlinear_solver': 'snes', 'snes_solver': {'linear_solver': 'mumps'}})


class Local_Data():
    pass


class Role():
    def __init__(self):
        self.worker = False
        self.master = False


comm = fenics.MPI.comm_world
mpi_size = comm.Get_size()
mpi_rank = comm.Get_rank()

role = Role()

if (mpi_size <= 1) or (mpi_rank >= 1):
    role.worker = True
if (mpi_size <= 1) or (mpi_rank <= 0):
    role.master = True


# for accessing the points objects as defined by fenics
class Fenics_Point(fenics.Point):
    # are already defined in fenics.Point :
    # ## array()
    # ## distance()
    # ## dot()
    # ## norm()
    # ## x()
    # ## y()
    # ## z()
    pass


# define an object called "Point" to store some information
# and define some associated methods
class Point():
    def from_x_y_theta_coordinates(self, x, y, theta_degree):
        # x and y coordinates on a vertical plane rotated by an angle theta
        r = x
        self.z = y
        self.y = r * numpy.sin(degree_to_rad(theta_degree))
        self.x = r * numpy.cos(degree_to_rad(theta_degree))
        self.coordinates = (self.x, self.y, self.z)
        self.fenics = Fenics_Point(self.x, self.y, self.z)
        self.x_y_theta_coordinates = (x, y, theta_degree)

    def from_x_y_z_coordinates(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.coordinates = (self.x, self.y, self.z)
        self.fenics = Fenics_Point(self.x, self.y, self.z)
        self.x_y_theta_coordinates = self.get_x_y_theta_coordinates()

    # when the object is created
    def __init__(self, x=None, y=None, z=None, theta_degree=None, name=None, id=None):
        if theta_degree is None:
            # assume carthesian coordinates (x, y, z)
            self.from_x_y_z_coordinates(x, y, z)
        else:
            # assume hemispheric planar coordinates
            self.from_x_y_theta_coordinates(x, y, theta_degree)

        if name is not None:
            self.name = name
        if id is None:
            if name is not None:
                self.id = name
        else:
            self.id = id

    # modify the way the class is represented, so that calling a point's name returns its coordinates
    def __repr__(self):
        return repr(self.coordinates)

    def get_x_y_theta_coordinates(self):
        r = numpy.sqrt(self.x**2 + self.y**2)
        x = r
        y = self.z
        if r != 0:
            theta = numpy.arcsin(self.y / r)
        else:
            theta = 0
        return (x, y, rad_to_degree(theta))

    # get the value of a function (default: Pressure_mmHg) at a given point
    def get(self, simulation, function=None):
        simulation_mesh_deformation_undone = False
        if simulation.mesh_is_deformed:
            simulation.undo_mesh_deformation()
            simulation_mesh_deformation_undone = True

        if function is None:
            function = simulation.pressure_mmHg

        try:
            return_value = function(self.coordinates)
        except:
            if simulation.log_level < 20:
                print("!! The point does not appear to be inside the domain...")
            return_value = numpy.nan

        if simulation_mesh_deformation_undone:
            simulation.apply_mesh_deformation()

        return return_value


# Data object, can contain just anything
class Data:
    def __init__(self):
        # save measurement conditions in a dictionary, separated from raw data
        self.conditions = {
            "quarter_mesh": {
                "loadweight_qm": [],
                "offset_angle_qm": [],
                "offset_height_qm": [],
                "n_vector_qm": [],
                "top_point_displacement_qm": []
            },
            "full_mesh": {
                "loadweight": [],
                "offset_angle": [],
                "offset_height": [],
                "n_vector": [],
                "top_point_displacement": []
            },
        }

    # the Data object will be used to store measurement data and conditions
    # during iterative simulations...
    # results for full meshes and quarter meshes are stored separately
    # the following method automatically returns the right values for a given mesh
    def auto_conditions(self, simulation):
        # return the appropriate conditions_list depending on the mesh
        if simulation.quarter_mesh:
            return self.conditions["quarter_mesh"]
        else:
            return self.conditions["full_mesh"]


# Define an object called Sensors that is in fact a pack of predefined sensors
# Each sensor is a point, and can store data (and conditions)
# several sensors packs can be created with different offsets
# (offsets in z position (height) and/or rotation angle)
class Sensors:
    def update_data_dict_from_sensors(self):
        self.data.A = {
            "quarter_mesh": self.A_qm.data,
            "full_mesh": self.A.data
        }
        self.data.B = {
            "quarter_mesh": self.B_qm.data,
            "full_mesh": self.B.data
        }
        self.data.C = {
            "quarter_mesh": self.C_qm.data,
            "full_mesh": self.C.data
        }
        self.data.D = {
            "quarter_mesh": self.D_qm.data,
            "full_mesh": {
                "1": self.D1.data,
                "2": self.D2.data
            }
        }
        self.data.E = {
            "quarter_mesh": self.E_qm.data,
            "full_mesh": {
                "1": self.E1.data,
                "2": self.E2.data
            }
        }
        self.data.F = {
            "quarter_mesh": self.F_qm.data,
            "full_mesh": {
                "1": self.F1.data,
                "2": self.F2.data,
                "3": self.F3.data
            }
        }

    def __init__(self, offset_angle_degree=0, offset_height=0):
        self.offset_angle_degree = offset_angle_degree
        self.offset_height = offset_height
        # define sensors (name, id and coordinates)
        self.A_qm = Point(
            name='A',
            id='A_qm',
            x=0,
            y=0.055+offset_height,
            theta_degree=((0+offset_angle_degree)%90)
        )
        self.B_qm = Point(
            name='B',
            id='B_qm',
            x=0.010,
            y=0.030+offset_height,
            theta_degree=((0+offset_angle_degree)%90)
        )
        self.C_qm = Point(
            name='C',
            id='C_qm',
            x=0.005,
            y=0.010+offset_height,
            theta_degree=((0+offset_angle_degree)%90)
        )
        self.D_qm = Point(
            name='D',
            id='D_qm',
            x=0.030,
            y=0.045+offset_height,
            theta_degree=((45+offset_angle_degree)%90)
        )
        self.E_qm = Point(
            name='E',
            id='E_qm',
            x=0.030,
            y=0.010+offset_height,
            theta_degree=((45+offset_angle_degree)%90)
        )
        self.F_qm = Point(
            name='F',
            id='F_qm',
            x=0.050,
            y=0.020+offset_height,
            theta_degree=((0+offset_angle_degree)%90)
        )
        self.A = Point(
            name='A',
            x=0,
            y=0.055+offset_height,
            theta_degree=0+offset_angle_degree
        )
        self.B = Point(
            name='B',
            x=-0.010,
            y=0.030+offset_height,
            theta_degree=0+offset_angle_degree
        )
        self.C = Point(
            name='C',
            x=0.005,
            y=0.010+offset_height,
            theta_degree=0+offset_angle_degree
        )
        self.D1 = Point(
            name='D1',
            x=0.030,
            y=0.045+offset_height,
            theta_degree=45+offset_angle_degree
        )
        self.D2 = Point(
            name='D2',
            x=-0.030,
            y=0.045+offset_height,
            theta_degree=135+offset_angle_degree
        )
        self.E1 = Point(
            name='E1',
            x=-0.030,
            y=0.010+offset_height,
            theta_degree=45+offset_angle_degree
        )
        self.E2 = Point(
            name='E2',
            x=0.030,
            y=0.010+offset_height,
            theta_degree=135+offset_angle_degree
        )
        self.F1 = Point(
            name='F1',
            x=0.050,
            y=0.020+offset_height,
            theta_degree=0+offset_angle_degree
        )
        self.F2 = Point(
            name='F2',
            x=0.050,
            y=0.020+offset_height,
            theta_degree=90+offset_angle_degree
        )
        self.F3 = Point(
            name='F3',
            x=-0.050,
            y=0.020+offset_height,
            theta_degree=90+offset_angle_degree
        )
        # Create a dictionary containing all sensors
        self.sensors_list = {
            "quarter_mesh": [
                self.A_qm,
                self.B_qm,
                self.C_qm,
                self.D_qm,
                self.E_qm,
                self.F_qm
            ],
            "full_mesh": [
                self.A,
                self.B,
                self.C,
                self.D1,
                self.D2,
                self.E1,
                self.E2,
                self.F1,
                self.F2,
                self.F3
            ],
            "all_sensors": [
                self.A_qm,
                self.B_qm,
                self.C_qm,
                self.D_qm,
                self.E_qm,
                self.F_qm,
                self.A,
                self.B,
                self.C,
                self.D1,
                self.D2,
                self.E1,
                self.E2,
                self.F1,
                self.F2,
                self.F3
            ]
        }
        # create a data list for each sensor
        for s in self.sensors_list["all_sensors"]:
            s.data = []
        # link all the data lists into a "data" object
        self.data = Data()
        self.update_data_dict_from_sensors()

    def auto_sensors_list(self, simulation):
        # return the appropriate sensors_list depending on the mesh
        if simulation.quarter_mesh:
            return self.sensors_list["quarter_mesh"]
        else:
            return self.sensors_list["full_mesh"]

    # get the value of a specific function for all the sensors in the pack
    def get(self, simulation, function=None):
        simulation_mesh_deformation_undone = False
        sensors_list = self.auto_sensors_list(simulation)

        if simulation.mesh_is_deformed:
            simulation.undo_mesh_deformation()
            simulation_mesh_deformation_undone = True

        if function is None:
            function = simulation.pressure_mmHg

        for i in sensors_list:
            print(f"{i.name:s}:\t{i.get(simulation, function):f}")
        print("")

        if simulation_mesh_deformation_undone:
            simulation.apply_mesh_deformation()

    # the following functions allow updating the sensors position
    # to avoid re-creating a new sensors pack everytime...
    # this also means that all the data can be stored in one place
    def change_offset_angle(self, offset_angle_degree):
        for s in self.sensors_list["quarter_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, y, (theta_degree + offset_angle_degree - self.offset_angle_degree) % 90)
        for s in self.sensors_list["full_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, y, theta_degree + offset_angle_degree - self.offset_angle_degree)
        self.offset_angle_degree = offset_angle_degree

    def change_offset_height(self, offset_height):
        for s in self.sensors_list["quarter_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, (y + offset_height - self.offset_height), (theta_degree) % 90)
        for s in self.sensors_list["full_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, (y + offset_height - self.offset_height), theta_degree)
        self.offset_height = offset_height

    def replace(self, offset_angle_degree=None, offset_height=None):
        if offset_angle_degree is None:
            offset_angle_degree = self.offset_angle_degree
        if offset_height is None:
            offset_height = self.offset_height
        for s in self.sensors_list["quarter_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, (y + offset_height - self.offset_height), (theta_degree + offset_angle_degree - self.offset_angle_degree) % 90)
        for s in self.sensors_list["full_mesh"]:
            x, y, theta_degree = s.x_y_theta_coordinates
            s.from_x_y_theta_coordinates(x, (y + offset_height - self.offset_height), (theta_degree + offset_angle_degree - self.offset_angle_degree))
        self.offset_angle_degree = offset_angle_degree
        self.offset_height = offset_height


default_sensors = Sensors(offset_angle_degree=sensors_offset_angle_default, offset_height=sensors_offset_height_default)


# Main object of this file: a "New Simulation" object that can initialize
# and contain a whole new simulation with new parameters
# this allows data sharing and at the same time allows several simulations
# to be created "simultaneously" (within the limits of hardware capacity...)
# The most basic usage of this is :
# 1) sim = New_Simulation(<parameters>)
# 2) sim.compute()
class New_Simulation:

    # Set constants to be used in calculations and not depending on the mesh
    def set_instance_constants(
        self,
        loadweight=loadweight_default,
        loadweight_offset=loadweight_offset_default,
        quarter_mesh=quarter_mesh_default,
        log_level=log_level_default,
        plot_level=plot_level_default,
        use_vacuum=use_vacuum_default,
        solver=solver_default,
        g=None,
        mesh_file=None
    ):
        if log_level < 30:
            print(">> Setting up conditions...")

        current_latitude = 36.40  # in degrees, Ueda, Japan

        self.loadweight = loadweight
        if not use_vacuum:
            self.loadweight_offset = loadweight_offset
        else:
            self.loadweight_offset = 0
        self.quarter_mesh = quarter_mesh
        self.log_level = log_level
        self.plot_level = plot_level
        self.use_vacuum = use_vacuum
        self.solver = solver
        self.mesh_file = mesh_file
        if g is None:
            self.g = 9.806 - 1/2*(9.832 - 9.780)*numpy.cos(2*current_latitude*numpy.pi/180)
        else:
            self.g = g

    # Set loglevel, note: DBG=10, TRACE=13, PROGRESS=16, INFO=20, WARNING=30, ERROR=40, CRITICAL=50,
    # set_log_active(False) for no log at all
    def set_fenics_log_level(self, log_level=None):
        if log_level is None:
            log_level = self.log_level
        if log_level < 30:
            print(">> Setting up fenics log level...")
        fenics.set_log_level(log_level)

    # Set the numerical constants for the material properties.
    # ToDo: add the possibility to specify values from arguments
    def set_material_properties(
        self,
        rho_values=rho_values_default,
        young_modulus_values=young_modulus_values_default,
        poisson_ratio_values=poisson_ratio_values_default
    ):
        # Scaled constants
        # if given as arrays, values are in order : [Skin, Adipose, Glanduar]
        # if two layers have the same values, they are supposed to behave as one layer
        if self.log_level < 30:
            print(">> Setting up material properties...")
        self.rho_values = numpy.asarray(rho_values)  # in kg/m^3
        self.young_modulus_values = numpy.asarray(young_modulus_values)  # in Pa
        self.poisson_ratio_values = numpy.asarray(poisson_ratio_values)  # no unit
        self.lambda__values = (self.young_modulus_values*self.poisson_ratio_values)/((1+self.poisson_ratio_values)*(1-2*self.poisson_ratio_values))  # in Pa, normally
        self.mu_values = (self.young_modulus_values)/(2*(1+self.poisson_ratio_values))  # in Pa, normally

    # Convert a ".msh" mesh into xdmf(s) with subdomain markers
    def extract_mesh_information(self, filename=None):
        if self.log_level < 30:
            print(">> Extracting mesh information...")
        # read the 3D GMsh file:
        if filename:
            msh = meshio.read(filename)
        else:
            if self.quarter_mesh:
                msh = meshio.read("Mesh_5_quarter_only.msh")
            else:
                msh = meshio.read("Mesh_5.msh")
        tetra_cells = None
        triangle_cells = None
        self.mesh_has_tetra_cells = False
        self.mesh_has_triangle_cells = False
        for key in msh.cell_data_dict["gmsh:physical"].keys():
            if key == "tetra":
                tetra_data = msh.cell_data_dict["gmsh:physical"][key]
                tetra_cells = msh.cells_dict[key]
            elif key == "triangle":
                triangle_data = msh.cell_data_dict["gmsh:physical"][key]
                triangle_cells = msh.cells_dict[key]
        if tetra_cells is not None:
            self.mesh_has_tetra_cells = True
            tetra_mesh = meshio.Mesh(points=msh.points, cells={"tetra": tetra_cells}, cell_data={"name_to_read":[tetra_data]})
            meshio.write("generated/mesh.xdmf", tetra_mesh)
        else:
            print("!! Error: a mesh containing tetrahedrons is expected !")
            exit(2)
        if triangle_cells is not None:
            self.mesh_has_triangle_cells = True
            triangle_mesh = meshio.Mesh(points=msh.points, cells=[("triangle", triangle_cells)], cell_data={"name_to_read":[triangle_data]})
            meshio.write("generated/mf.xdmf", triangle_mesh)
        # this whole function should only be executed by the master node
        if role.master:
            self.broadcast_data = {
                "mesh_has_tetra_cells": self.mesh_has_tetra_cells,
                "mesh_has_triangle_cells": self.mesh_has_triangle_cells
            }
            #self.broadcast_data = comm.bcast(self.broadcast_data, root=0)
            if mpi_size > 1:
                for receiver in range(1, mpi_size):
                    comm.send(self.broadcast_data, dest=receiver, tag=1)
                    #print("sent to ", receiver)
                    sys.stdout.flush()

    # read the mesh from xdmf files
    def read_extracted_mesh_and_subdomains(self):
        if not role.master:
            self.broadcast_data = comm.recv(source=0, tag=1)
        #print("received:", self.broadcast_data)
        self.mesh_has_tetra_cells = self.broadcast_data["mesh_has_tetra_cells"]
        self.mesh_has_triangle_cells = self.broadcast_data["mesh_has_triangle_cells"]
        if self.log_level < 30:
            print(">> Reading extracted mesh and subdomains...")
        # declare a mesh and read its properties from the previously created files
        # note that for now only the tetrahedron cells are used
        # (which is probably ok but results in a "rougher" external surface)
        self.mesh = fenics.Mesh()
        mvc2 = fenics.MeshValueCollection("size_t", self.mesh, 3)
        with fenics.XDMFFile(self.mesh.mpi_comm(), "generated/mesh.xdmf") as infile:
            infile.read(self.mesh)
            infile.read(mvc2, "name_to_read")
        if self.mesh_has_triangle_cells:
            mvc = fenics.MeshValueCollection("size_t", self.mesh, 2)
            with fenics.XDMFFile("generated/mf.xdmf") as infile:
                infile.read(mvc, "name_to_read")
        # define subdomains (different tissues) from the imported mesh, as a function of the mesh
        # note: the subdomains were given tags ("0", "1", "2") in GMsh during the mesh creation
        # then the tags were written in the "mvc" variable for all the tetrahedrons in the mesh
        self.surfaces = fenics.cpp.mesh.MeshFunctionSizet(self.mesh, mvc)
        self.subdomains = fenics.cpp.mesh.MeshFunctionSizet(self.mesh, mvc2)

        # now that the mesh has been read, declare a variable to keep a trace of applied deformations
        self.mesh_is_deformed = False

    def get_points_coordinates(self):
        if self.log_level < 30:
            print(">> Getting mesh points coordinates...")
        # array containing coordinates of all the points in the mesh
        # the shape of the array is: [cellnum, pointnum, coordinate]
        # with pointnum going from 0 to 3 (4 points in each tetra cell)
        # and coordinates as follows: x->0 , y->1 , z->2
        self.cells_points_coordinates = numpy.asarray(self.mesh.coordinates()[self.mesh.cells()])
        # also create an array containing the (virtual) "midpoint" coordinates
        # for each cell (by calculating the average of all the points of each cell)
        self.cells_midpoint_coordinates = self.cells_points_coordinates.mean(axis=1)

    # variables depending on the mesh, such as the radius...
    def set_mesh_dependant_variables(self):
        if self.log_level < 30:
            print(">> Setting up mesh-dependant variables...")
        # The imported mesh was already scaled.
        # In order to avoid hard-coding the scale value, calculate it from coordinates
        # Note that it is supposed here that we use a quarter (1/4) of a hemisphere or a full hemisphere
        skin_max_coordinate = self.cells_points_coordinates[self.subdomains.array()==0].max()
        adipose_max_coordinate = self.cells_points_coordinates[self.subdomains.array()==1].max()
        glandular_max_coordinate = self.cells_points_coordinates[self.subdomains.array()==2].max()
        origin = numpy.abs(self.mesh.coordinates()[:, :]).min()
        self.model_radius = numpy.abs(self.mesh.coordinates()[:, :]).max() - origin
        self.skin_thickness = skin_max_coordinate - adipose_max_coordinate
        self.adipose_min_thickness = adipose_max_coordinate - glandular_max_coordinate
        self.glandular_max_thickness = glandular_max_coordinate - origin

    # Can only be initialized after the mesh was read or the radius set manually
    def initialize_and_scale_3D_plot(self):
        # The following is used for plotting points or the whole mesh in 3D
        if (self.plot_level > 0):
            if self.log_level < 30:
                print(">> Initializing and scaling 3D plot...")
            fig = plt.figure()
            self.ax = Axes3D(fig)
            # Scale the 3D plot to the dimensions of the model
            if self.quarter_mesh:
                self.ax.set_xlim3d(0, self.model_radius)
                self.ax.set_ylim3d(0, self.model_radius)
                self.ax.set_zlim3d(0, self.model_radius)
            else:
                self.ax.set_xlim3d(-self.model_radius, self.model_radius)
                self.ax.set_ylim3d(-self.model_radius, self.model_radius)
                self.ax.set_zlim3d(-self.model_radius, self.model_radius)

    # print set material properties and mesh properties in a readable format
    def print_initial_problem_data(self):
        # Print the problem data in a clear and readable format
        print("\n########################################################")
        print("Problem data:")
        print(f'Skin:\n\tρ = {self.rho_values[0]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[0]:f} Pa\n\tν = {self.poisson_ratio_values[0]:f} (ratio)\n\tλ = {self.lambda__values[0]:f} Pa\n\tμ = {self.mu_values[0]:f} Pa')
        print(f'Adipose tissue:\n\tρ = {self.rho_values[1]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[1]:f} Pa\n\tν = {self.poisson_ratio_values[1]:f} (ratio)\n\tλ = {self.lambda__values[1]:f} Pa\n\tμ = {self.mu_values[1]:f} Pa')
        print(f'Glandular tissue:\n\tρ = {self.rho_values[2]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[2]:f} Pa\n\tν = {self.poisson_ratio_values[2]:f} (ratio)\n\tλ = {self.lambda__values[2]:f} Pa\n\tμ = {self.mu_values[2]:f} Pa')
        print(f'Gravity constant:\n\tg = {self.g:f} m・s^(-2)')
        print(f'Model scale: \n\tradius = {self.model_radius:f} m')
        print(f'Mesh properties:\n\t{self.mesh.num_vertices():d} points\n\t{self.mesh.num_cells():d} cells')
        if (self.loadweight == 0):
            print(f'Load condition: \n\tno load')
        else:
            if self.use_vacuum:
                # to be converted into Pascal, psi or mmHg ?
                print(f'Applied vacuum: \n\tload = {self.loadweight:f} kg')
            else:
                print(f'Load condition: \n\tload = {self.loadweight:f} kg')
        if (self.loadweight_offset != 0):
            print(f'Offset load: \n\tload = {self.loadweight_offset:f} kg')
        print("\n########################################################\n\n")

    ##############################################################################
    # The following block is to define all the required function spaces
    ##############################################################################
    def define_function_spaces(self):
        if self.log_level < 30:
            print(">> Defining FEniCS function spaces...")
        # define the vector function space to be used for computing displacement u
        # required here since the following boundary conditions
        # are setting limits to (some components of) the displacement
        # the vectors defined in this function space are applied point-wise
        # (a 3D vector function space of degree 1 has 3 times the dimension of a function space)
        self.PointsVectorFuncSpace = fenics.VectorFunctionSpace(self.mesh, 'P', 1)  # or 'CG'
        # define a function space to be used for storing 1-dimensional values point-wise
        self.PointsFuncSpace = fenics.FunctionSpace(self.mesh, 'P', 1)  # or 'CG'
        # define a Tensor function space to be used for the computation of internal pressure
        # this one is defined cell-wise, and the function is supposed discontinuous
        self.CellsTensorFuncSpace = fenics.TensorFunctionSpace(self.mesh, 'DP', 0)
        # define a function space to be used for storing 1-dimensional values cell-wise
        # again this one is supposed discontinuous
        self.CellsFuncSpace = fenics.FunctionSpace(self.mesh, 'DP', 0)  # or 'DG'
        # same but for vector FunctionS
        self.CellsVectorFuncSpace = fenics.VectorFunctionSpace(self.mesh, 'DP', 0)

    ##############################################################################
    # The following block is to define boundary conditions
    ##############################################################################
    def define_boundary_conditions(self):
        if self.log_level < 30:
            print(">> Defining boundary conditions...")
        tol = 1E-14 * self.model_radius

        # note that for the following, "on_boundary" is a function defined automatically
        # by fenics, and that returns True if a point is on the outermost layer of the mesh
        # define the bottom boundary as z (=x[2] for fenics) near 0
        def bottom_boundary(x, on_boundary):
            return on_boundary and fenics.near(x[2], 0, tol)

        # define the left boundary as y (=x[1] for fenics) near 0
        def left_boundary(x, on_boundary):
            return on_boundary and fenics.near(x[1], 0, tol)

        # define the right boundary as x (=x[0] for fenics) near 0
        def right_boundary(x, on_boundary):
            return on_boundary and fenics.near(x[0], 0, tol)

        # define the external surface boundary (for load application)
        # here we consider not only the surface of the mesh,
        # but the actual surface of the breast model
        if self.quarter_mesh:
            def exterior_boundary(x, on_boundary):
                # bottom "surface" excluded (z near 0)
                if not fenics.near(x[2], 0, tol):
                    # left "surface" excluded (y near 0)
                    if not fenics.near(x[1], 0, tol):
                        # right "surface" excluded (x near 0)
                        if not fenics.near(x[0], 0, tol):
                            # return True if on the surface of the mesh
                            return on_boundary
                return False
        else:
            def exterior_boundary(x, on_boundary):
                # bottom "surface" excluded (z near 0)
                if not fenics.near(x[2], 0, tol):
                    return on_boundary
                return False

        # actual definition of the boundary conditions
        # prevent any displacement on the bottom plane, component-wise
        # .sub(0) means the first component of the vector in the Vector Function Space (basically x)
        bc_bottom_x = fenics.DirichletBC(self.PointsVectorFuncSpace.sub(0), fenics.Constant(0), bottom_boundary)
        bc_bottom_y = fenics.DirichletBC(self.PointsVectorFuncSpace.sub(1), fenics.Constant(0), bottom_boundary)
        bc_bottom_z = fenics.DirichletBC(self.PointsVectorFuncSpace.sub(2), fenics.Constant(0), bottom_boundary)
        if self.quarter_mesh:
            # the left plane is along the y axis (external surface towards the back)
            # prevent crossing the y axis ("wall-like" boundary since the cut is virtual)
            bc_left_y = fenics.DirichletBC(self.PointsVectorFuncSpace.sub(1), fenics.Constant(0), left_boundary)
            # the right plane is along the x axis, so prevent crossing the x axis ("wall")
            bc_right_x = fenics.DirichletBC(self.PointsVectorFuncSpace.sub(0), fenics.Constant(0), right_boundary)

        # assembling most of the previously defined boundary conditions into one set
        # to be used as boundary conditions for the displacement computation
        # Delete bc_bottom_x and bc_bottom_y to allow lateral displacement on the bottom plane
        if self.quarter_mesh:
            self.bc = [bc_bottom_x, bc_bottom_y, bc_bottom_z, bc_left_y, bc_right_x]
        else:
            self.bc = [bc_bottom_x, bc_bottom_y, bc_bottom_z]

        # declaration of the boundary condition for the external surface
        # gives a value of 1 to the points on the boundary (NaN or 0 otherwise?)
        self.bc_ext = fenics.DirichletBC(self.PointsFuncSpace, 1, exterior_boundary)

    ##############################################################################
    # The following block is to define functions used in the computation
    ##############################################################################
    def assign_material_properties_to_cells(self):
        if self.log_level < 30:
            print(">> Assigning material properties to corresponding sublayers...")
        # mu, rho and lambda are functions setting different material properties
        # to the different layers of the mesh (skin, adipose, glandular)
        # they set one value per cell and are thus defined in the CellsFuncSpace
        self.mu = fenics.Function(self.CellsFuncSpace)
        self.rho = fenics.Function(self.CellsFuncSpace)
        self.lambda_ = fenics.Function(self.CellsFuncSpace)
        # The following functions are added in order to get extra information on points
        self.young_modulus = fenics.Function(self.CellsFuncSpace)
        self.poisson_ratio = fenics.Function(self.CellsFuncSpace)
        self.subdomain = fenics.Function(self.CellsFuncSpace)
        # define an array to contain the subdomain values
        # in a compatible format for later operations (int64 instead of uint64)
        subdomain_values = numpy.asarray(self.subdomains.array(), dtype=numpy.int64)
        # give different material properties to each cell according to the subdomain tags
        # subdomain_values contains the tags ("0", "1", or "2") given by GMsh
        # rho_values, mu_values and lambda__values are of the shape: [<v0>, <v1>, <v2>]
        # numpy.choose() picks <v0> if the cell's tag is 0, <v1> if the tag is 1, etc.
        # the values are stored into a "vector" (1-dimensional array) of the corresponding function
        self.mu.vector()[:] = numpy.choose(subdomain_values, self.mu_values)
        self.rho.vector()[:] = numpy.choose(subdomain_values, self.rho_values)
        self.lambda_.vector()[:] = numpy.choose(subdomain_values, self.lambda__values)
        # The following functions are added in order to get extra information on points
        self.young_modulus.vector()[:] = numpy.choose(subdomain_values, self.young_modulus_values)
        self.poisson_ratio.vector()[:] = numpy.choose(subdomain_values, self.poisson_ratio_values)
        self.subdomain.vector()[:] = subdomain_values

    ##############################################################################
    # This bloc is for detecting whether a cell is on the outermost layer
    ##############################################################################
    def detect_external_cells(self):
        if self.log_level < 30:
            print(">> Detecting external cells...")
        # test_ext is a function that will return 1 if a point is on the exterior boundary, 0 otherwise
        test_ext_point = fenics.Function(self.PointsFuncSpace)
        # initializing to 0, just to make sure
        test_ext_point.vector()[:] = 0.
        # applying the bc_ext boundary condition on this function
        # to fill its "vector" of point-wise values with 1 when on boundary
        self.bc_ext.apply(test_ext_point.vector())
        # now create a function over cells, and projecting test_ext on it
        # the result should be different from 0 if a cell contains points on the external boundary, 0 otherwise
        # note that a tetrahedron cell has 4 points, since the value was "projected"
        # from a point-wise space to a cell-wise space, the value given to a cell
        # is the average of the values of its points :
        # 1 if all the points are on the boundary (which would probably not happen)
        # 0.75 if 3 points out of 4 are on the boundary; 0.5 if only 2 points, etc.
        # #self.cell_is_external = fenics.Function(self.CellsFuncSpace)
        self.cell_is_external = fenics.project(test_ext_point, self.CellsFuncSpace)
        # However, maybe it would be better if this function simply returned 1
        # if the cell belongs to the outermost layer and 0 otherwise
        # Thus, the following is to correct the output of the function
        # (meaning: where the vector is nonzero, fill with 1)
        self.cell_is_external.vector()[(self.cell_is_external.vector()[:]).nonzero()[0]] = 1

    ###############################################################################
    # This bloc defines the forces applied over the whole volume (weight only?)
    ###############################################################################
    def define_forces_over_volume(self):
        if self.log_level < 30:
            print(">> Defining forces applied over the whole volume...")

        # in a former attempt to formulate the weight of each cell independently,
        # the following function was defined, but it turns out that the calculation
        # should be made using forces per unit of volume, which discards this idea...
        # kept however as an example of how to get the cells volume information
        #  #ownweight = rho*fenics.CellVolume(mesh)*g
        #  #ownweight = fenics.project(ownweight, CellsFuncSpace)

        # functions defining the forces applied to the internal volume (!) in the 3 (separate) dimensions
        fv_x = fenics.Function(self.CellsFuncSpace)
        fv_y = fenics.Function(self.CellsFuncSpace)
        fv_z = fenics.Function(self.CellsFuncSpace)

        # initialization
        fv_x.vector()[:] = 0  # might change if in standing position
        fv_y.vector()[:] = 0  # might change if in standing position
        fv_z.vector()[:] = - self.rho.vector()[:]*self.g  # gravity in the z direction, towards the bottom

        # Assembly of the fs_x, fs_y and fs_z functions into a single vector (namedtuple)
        # #fv = fenics.dot(ufl.as_vector(numpy.asarray([fv_x, fv_y, fv_z])), fenics.Identity(3))
        # #fv = ufl.as_vector([fv_x, fv_y, fv_z])
        # can be later adressed as fv.x, fv.y and fv.z
        self.fv = vector(fv_x, fv_y, fv_z)  # shall be converted to ufl later for the calculation

    def define_forces_over_surface(self):
        if self.log_level < 30:
            print(">> Defining constant forces applied on the external surface...")

        # functions defining the external forces (pressures?) applied to the surface (!) in the 3 (separate) dimensions
        Ps_x = fenics.Function(self.CellsFuncSpace)
        Ps_y = fenics.Function(self.CellsFuncSpace)
        Ps_z = fenics.Function(self.CellsFuncSpace)

        # define a function for identifying the cells exposed to the load
        self.cell_has_contact_with_load = fenics.Function(self.CellsFuncSpace)

        # initialization of the x and y component vectors
        Ps_x.vector()[:] = 0  # might change if in standing position
        Ps_y.vector()[:] = 0  # might change if in standing position
        Ps_z.vector()[:] = 0  # only for the sake of initializing values

        # Assembly of the Ps_x, Ps_y and Ps_z functions into a single vector (namedtuple)
        # each component can later be adressed as Ps.x, Ps.y and Ps.z
        self.Ps = vector(Ps_x, Ps_y, Ps_z)  # shall be converted to ufl later for the calculation

    # ################################################################################
    # This bloc is used for getting the properties of the exterior facets of the mesh
    # ################################################################################
    def get_exterior_facets_properties(self):

        local = Local_Data()

        if self.log_level < 30:
            print(">> Getting exterior facets properties...")

        self.cell_has_external_facet = fenics.Function(self.CellsFuncSpace)
        self.cell_has_external_facet.vector()[:] = 0.
        local.cell_has_external_facet = numpy.zeros(self.cell_has_external_facet.vector()[:].shape)
        self.external_facet_surface_area = fenics.Function(self.CellsFuncSpace)
        self.external_facet_surface_area.vector()[:] = 0.
        local.external_facet_surface_area = numpy.zeros(self.external_facet_surface_area.vector()[:].shape)
        # Is the normal vector only needed when applying vacuum ?
        external_facet_nvector_x = fenics.Function(self.CellsFuncSpace)
        external_facet_nvector_y = fenics.Function(self.CellsFuncSpace)
        external_facet_nvector_z = fenics.Function(self.CellsFuncSpace)
        external_facet_nvector_x.vector()[:] = 0.
        external_facet_nvector_y.vector()[:] = 0.
        external_facet_nvector_z.vector()[:] = 0.
        local.external_facet_nvector_x = numpy.zeros(external_facet_nvector_x.vector()[:].shape)
        local.external_facet_nvector_y = numpy.zeros(external_facet_nvector_y.vector()[:].shape)
        local.external_facet_nvector_z = numpy.zeros(external_facet_nvector_z.vector()[:].shape)

        self.external_facets_index = []

        #comm.barrier()
        for facet in range(0, self.mesh.num_facets()):
            if fenics.Facet(self.mesh, facet).exterior():
                # keep the index number of the facet for faster update later
                self.external_facets_index.append(facet)
                # little shorthand for readability
                associated_cell = fenics.Facet(self.mesh, facet).entities(3)[0]
                # mark the corresponding cell as external
                local.cell_has_external_facet[associated_cell] = 1
                # calculate the value of the exterior facet's surface area
                facet_point_A, facet_point_B, facet_point_C = self.mesh.coordinates()[(fenics.Facet(self.mesh, facet).entities(0)[:])]
                local.external_facet_surface_area[associated_cell] = triangle_area([facet_point_A, facet_point_B, facet_point_C])
                # calculate the components of the normal vector passing by the facet's midpoint
                facet_midpoint = fenics.Facet(self.mesh, facet).midpoint()[:]
                facet_nvector = cross_product((facet_point_A - facet_midpoint), (facet_point_B - facet_midpoint))
                # if the vector is pointed inwards (closer to the cell's midpoint), invert its direction
                if (vector_norm((facet_midpoint + facet_nvector) - self.cells_midpoint_coordinates[associated_cell]) < vector_norm((facet_midpoint - facet_nvector) - self.cells_midpoint_coordinates[associated_cell])):
                    facet_nvector *= -1
                # now scale the vector to ensure its norm is 1, and associate it with the appropriate cell
                facet_nvector /= vector_norm(facet_nvector)
                local.external_facet_nvector_x[associated_cell] = facet_nvector[0]
                local.external_facet_nvector_y[associated_cell] = facet_nvector[1]
                local.external_facet_nvector_z[associated_cell] = facet_nvector[2]
            # seems like we're done with the facet by facet analysis here.
            # The cells with no exterior facet are left with a value of 0.
        # Copy the local data into the simulation's function spaces
        self.cell_has_external_facet.vector()[:] = local.cell_has_external_facet[:].copy()
        self.external_facet_surface_area.vector()[:] = local.external_facet_surface_area[:].copy()
        external_facet_nvector_x.vector()[:] = local.external_facet_nvector_x[:].copy()
        external_facet_nvector_y.vector()[:] = local.external_facet_nvector_y[:].copy()
        external_facet_nvector_z.vector()[:] = local.external_facet_nvector_z[:].copy()
        # Adjust the functions that were just created to take into account the selected boundaries (cell_is_external)
        self.cell_has_external_facet.vector()[:] *= self.cell_is_external.vector()[:]
        self.external_facet_surface_area.vector()[:] *= self.cell_is_external.vector()[:]
        external_facet_nvector_x.vector()[:] *= self.cell_is_external.vector()[:]
        external_facet_nvector_y.vector()[:] *= self.cell_is_external.vector()[:]
        external_facet_nvector_z.vector()[:] *= self.cell_is_external.vector()[:]

        # to be converted to ufl later for calculation
        self.external_facet_nvector = vector(external_facet_nvector_x, external_facet_nvector_y, external_facet_nvector_z)

    def prepare_for_calculation(self):
        if self.log_level < 30:
            print(">> Preparing for calculation...")

        # For the definition of the variational problem
        # A trial function and a test function (for the variational formulation)
        # are defined over the PointsVectorFuncSpace as the main calculation is about
        # the displacement vector associated with each point
        # The Trial Function u i the unknown function to be found as a solution of the Partial Differential Equation
        # The test function is a (variational) defined function that should be optimized
        # to yield the "best" or "finest" approximation of u.
        # It exists in a space defined over the mesh and is limited by the boundaries
        # (at least in my current understanding of the variational method)
        self.u = fenics.TrialFunction(self.PointsVectorFuncSpace)
        self.v = fenics.TestFunction(self.PointsVectorFuncSpace)
        # Function to compute and store the solution: define u_computed as a point-wise vector function
        self.u_computed = fenics.Function(self.PointsVectorFuncSpace)
        # Function for canceling changes applied by u_computed
        self.cancel_u_computed = fenics.Function(self.PointsVectorFuncSpace)

    # variable initialized, modified in each instance with weight updates
    load_balance_z_criterion = 0

    # variables and functions that should not be used when dealing with vacuum ?
    def prepare_for_load_balancing(self):
        if self.log_level < 30:
            print(">> Preparing for load balancing...")

        # distance between the top point and the other selected points, in m
        # used for the selection of the cells exposed to the load
        # 0 would select only the cell(s) with the highest midpoint at each iteration
        self.load_balance_z_criterion_min = 0
        self.load_balance_z_criterion_max = self.model_radius
        # not needed anymore?
        #self.load_balance_z_criterion = (self.load_balance_z_criterion_max + self.load_balance_z_criterion_min) / 2  # in m

        # coefficients for the external weight scaling function...
        if self.quarter_mesh:
            # the mesh represents 1/4 of the actual breast model on which the full load is applied
            # so divide the load by 4 as we suppose it is spread symetrically over the 4 quarters of the model
            self.load_scaling = 1./4
        else:
            self.load_scaling = 1.

        if self.use_vacuum:
            self.max_iterations = 1
        else:
            if (self.loadweight == 0) and (self.loadweight_offset == 0):
                self.max_iterations = 1
            else:
                self.max_iterations = 50

    def update_loadweight(self, new_loadweight=None, new_loadweight_offset=None):
        if self.log_level < 30:
            print("")
        if new_loadweight is not None:
            if new_loadweight != self.loadweight:
                if self.log_level < 30:
                    print(f">> Updating load weight: {self.loadweight:f} kg --> {new_loadweight:f} kg")
                self.loadweight = new_loadweight
        if new_loadweight_offset is not None:
            if new_loadweight_offset != self.loadweight_offset:
                if self.log_level < 30:
                    print(f">> Updating load weight offset: {self.loadweight_offset:f} kg --> {new_loadweight_offset:f} kg")
                self.loadweight_offset = new_loadweight_offset
        if self.mesh_is_deformed:
            self.undo_mesh_deformation()
        self.prepare_for_load_balancing()
        if self.log_level < 30:
            print("")

    def update_cells_selection(self, load_balance_z_criterion):
        if self.log_level < 16:
            print(">>>> Updating cells selection...")

        # initialize or reinitialize the cells selection with a zero value (no cells selected)
        self.cell_has_contact_with_load.vector()[:] = 0.

        if self.use_vacuum:
            # if using vacuum, all the external cells are selected
            self.cell_has_contact_with_load.vector()[:] = self.cell_has_external_facet.vector()[:].copy()
        else:
            # select the cells on which the load is to be applied, by setting to 1
            # the "cell_has_contact_with_load" function value for the cells from the
            # highest midpoint coordinates and within the load_balance_z_criterion
            # (meaning: set cell_has_contact_with_load to 1 where the z coordinate
            # of the cell's midpoint is higher than the z coordinate of the highest
            # midpoint, minus the load_balance_z_criterion)
            self.cell_has_contact_with_load.vector()[(self.cells_midpoint_coordinates[:, 2] >= self.cells_midpoint_coordinates[self.cells_midpoint_coordinates[:, 2].argmax(), 2] - load_balance_z_criterion).nonzero()[0]] = 1
            # only keep the cells that are on the outermost layer
            self.cell_has_contact_with_load.vector()[:] *= self.cell_has_external_facet.vector()[:]
            # for clarity, save the selected cells' indexes in a separate array

        self.selected_cells = self.cell_has_contact_with_load.vector()[:].nonzero()[0]

    def update_load_application_surface(self):
        if self.log_level < 16:
            print(">>>> Updating load application surface...")
        # approximate the surface area on which the load is applied, as the sum
        # of the approximate surface areas of all the tetrahedrons bearing the load
        # this approximation depends on the tetra_radius_scaling parameter for fine-tuning
        # #load_application_surface = (cell_has_contact_with_load.vector()[:]*ellipse_area(tetra_radius_scaling*cells_points_coordinates.std(axis=1)[:, 0], tetra_radius_scaling*cells_points_coordinates.std(axis=1)[:, 1])).sum()
        self.load_application_surface = (self.cell_has_contact_with_load.vector()[:]*self.external_facet_surface_area.vector()[:]).sum()

    def update_load_vector(self):
        if self.log_level < 16:
            print(">>>> Updating load vector...")
        # defined as a force per exposed surface area
        # which actually makes it analogous to a pressure (seems accurate so far)
        self.Ps.z.vector()[:] = - self.cell_has_contact_with_load.vector()[:] * (self.load_scaling * (self.loadweight + self.loadweight_offset) * self.g) / (self.load_application_surface)
        if self.use_vacuum:
            # atmospheric pressure can be seen as directed along the normal to each facet
            self.Ps.x.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.x.vector()[:]
            self.Ps.y.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.y.vector()[:]
            self.Ps.z.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.z.vector()[:]

    def express_and_compute_variational_problem(self):
        ###########################################################################
        # At this point, the problem should have has been explicitely described
        # The following formulates a variational approach of the problem
        # And carries out the main computation task
        ###########################################################################

        if self.solver == "linear":
            # For now, solve the problem using a linear elastic model
            # the solver will save the results of the computation in u_computed
            solve_with_linear_model(self)
        elif self.solver == "neo-hookean":
            # currently being tested
            solve_with_neo_hookean_model(self)
        elif self.solver == "yeoh":
            # currently being tested
            solve_with_yeoh_model(self)
        else:
            print(f"!! The \"{self.solver:s}\" solver is not available !!")
        # defined for the purpose of reverting changes at each iteration within the optimization loop
        self.cancel_u_computed.vector()[:] = - self.u_computed.vector()[:]

    def apply_mesh_deformation(self):
        # Deform the mesh according to the result of the calculation
        if not self.mesh_is_deformed:
            fenics.ALE.move(self.mesh, self.u_computed)
            self.mesh_is_deformed = True

    def undo_mesh_deformation(self):
        # Undo the mesh deformation before restarting the loop or getting out
        # Since the coordinates have been updated,
        # keeping the deformed mesh any longer in unnecessary
        if self.mesh_is_deformed:
            fenics.ALE.move(self.mesh, self.cancel_u_computed)
            self.mesh_is_deformed = False

    def get_moved_points_coordinates(self):
        if self.log_level < 16:
            print(">>>> Getting points coordinates after deformation...")
        # get the new points' coordinates
        if self.mesh_is_deformed:
            self.moved_cells_points_coordinates = numpy.asarray(self.mesh.coordinates()[self.mesh.cells()])
            self.moved_cells_midpoint_coordinates = self.moved_cells_points_coordinates.mean(axis=1)

    # ################################################################################
    # This bloc is used for getting the properties of the exterior facets of the mesh
    # Supposedly after deforming the mesh...
    # ################################################################################
    def update_exterior_facets_properties(self):
        local = Local_Data()
        local.external_facet_surface_area = numpy.zeros(self.external_facet_surface_area.vector()[:].shape)
        local.external_facet_nvector_x = numpy.zeros(self.external_facet_nvector.x.vector()[:].shape)
        local.external_facet_nvector_y = numpy.zeros(self.external_facet_nvector.y.vector()[:].shape)
        local.external_facet_nvector_z = numpy.zeros(self.external_facet_nvector.z.vector()[:].shape)
        if self.log_level < 16:
            print(">>>> Updating exterior facets properties...")
        for facet in self.external_facets_index:
            # little shorthand for readability
            associated_cell = fenics.Facet(self.mesh, facet).entities(3)[0]
            # calculate the value of the exterior facet's surface area
            facet_point_A, facet_point_B, facet_point_C = self.mesh.coordinates()[(fenics.Facet(self.mesh, facet).entities(0)[:])]
            local.external_facet_surface_area[associated_cell] = triangle_area([facet_point_A, facet_point_B, facet_point_C])
            # calculate the components of the normal vector passing by the facet's midpoint
            facet_midpoint = fenics.Facet(self.mesh, facet).midpoint()[:]
            facet_nvector = cross_product((facet_point_A - facet_midpoint), (facet_point_B - facet_midpoint))
            # if the vector is pointed inwards (closer to the cell's midpoint), invert its direction
            if self.mesh_is_deformed:
                ref_coordinates = self.moved_cells_midpoint_coordinates[associated_cell]
            else:
                ref_coordinates = self.cells_midpoint_coordinates[associated_cell]
            if (vector_norm((facet_midpoint + facet_nvector) - ref_coordinates) < vector_norm((facet_midpoint - facet_nvector) - ref_coordinates)):
                facet_nvector *= -1
            # now scale the vector to ensure its norm is 1, and associate it with the appropriate cell
            facet_nvector /= vector_norm(facet_nvector)
            local.external_facet_nvector_x[associated_cell] = facet_nvector[0]
            local.external_facet_nvector_y[associated_cell] = facet_nvector[1]
            local.external_facet_nvector_z[associated_cell] = facet_nvector[2]
        # seems like we're done with the facet by facet analysis here.
        # The cells with no exterior facet are left with a value of 0.
        # Copy the local data into the simulation's function spaces
        self.external_facet_surface_area.vector()[:] = local.external_facet_surface_area[:]
        self.external_facet_nvector.x.vector()[:] = local.external_facet_nvector_x[:]
        self.external_facet_nvector.y.vector()[:] = local.external_facet_nvector_y[:]
        self.external_facet_nvector.z.vector()[:] = local.external_facet_nvector_z[:]
        # Adjust the functions that were just created to take into account the selected boundaries (cell_is_external)
        self.external_facet_surface_area.vector()[:] *= self.cell_is_external.vector()[:]
        self.external_facet_nvector.x.vector()[:] *= self.cell_is_external.vector()[:]
        self.external_facet_nvector.y.vector()[:] *= self.cell_is_external.vector()[:]
        self.external_facet_nvector.z.vector()[:] *= self.cell_is_external.vector()[:]

    def interactive_3D_plot(self):
        # Matplotlib plotting in order to follow the optimization process (optional)
        if (self.plot_level >= 2):
            self.ax.clear()
            self.ax.scatter(self.cells_midpoint_coordinates[self.selected_cells][:, 0], self.cells_midpoint_coordinates[self.selected_cells][:, 1], self.cells_midpoint_coordinates[self.selected_cells][:, 2], marker='o', c='darkblue')
            self.ax.scatter(self.moved_cells_midpoint_coordinates[self.selected_cells][:, 0], self.moved_cells_midpoint_coordinates[self.selected_cells][:, 1], self.moved_cells_midpoint_coordinates[self.selected_cells][:, 2], marker='o', c='red')
        # Show the plot, but the computation will be hanging until Ctrl+c is pressed
        plt.show()

    def problem_definition_and_initialization(self):
        # execute each initialization function one by on in the right order
        if role.master:
            initialize_matplotlib(plot_level=self.plot_level, log_level=self.log_level)
            self.set_fenics_log_level()
        self.set_material_properties()
        if role.master:
            self.extract_mesh_information(filename=self.mesh_file)
        self.read_extracted_mesh_and_subdomains()
        self.get_points_coordinates()
        self.set_mesh_dependant_variables()
        if role.master:
            self.initialize_and_scale_3D_plot()
        self.print_initial_problem_data()
        self.define_function_spaces()
        self.define_boundary_conditions()
        self.assign_material_properties_to_cells()
        self.detect_external_cells()
        self.define_forces_over_volume()
        self.define_forces_over_surface()
        self.get_exterior_facets_properties()
        self.prepare_for_calculation()
        self.prepare_for_load_balancing()

    def stabilize_load_application_surface(self):
        # Initialization
        self.update_exterior_facets_properties()
        self.update_load_application_surface()
        self.update_load_vector()
        prev_load_application_surface = None
        # Stabilization loop
        while True:
            self.express_and_compute_variational_problem()
            self.apply_mesh_deformation()
            self.get_moved_points_coordinates()
            self.update_exterior_facets_properties()
            self.undo_mesh_deformation()
            self.update_load_application_surface()
            self.update_load_vector()
            if self.log_level < 16:
                print("surface =", self.load_application_surface)
            if prev_load_application_surface is not None:
                if numpy.abs(self.load_application_surface - prev_load_application_surface)/self.load_application_surface < 0.00001:
                    break
            prev_load_application_surface = self.load_application_surface

    # test a z_criterion (for load application surface selection) and returns the average n_vector
    def test_load_z_criterion(self, load_balance_z_criterion):
        self.update_cells_selection(load_balance_z_criterion)
        self.stabilize_load_application_surface()
        n_vector_averages = numpy.zeros(mpi_size, dtype=float)
        n_vector_averages[:] = numpy.asarray(self.external_facet_nvector.z.vector()[self.selected_cells]).mean()
        n_vector_populations = numpy.zeros(mpi_size, dtype=int)
        n_vector_populations[:] = numpy.asarray(self.external_facet_nvector.z.vector()[self.selected_cells]).size
        n_vector_averages = comm.alltoall(sendobj=n_vector_averages)
        n_vector_populations = comm.alltoall(sendobj=n_vector_populations)
        n_vector_global_average = (numpy.asarray(n_vector_averages) * numpy.asarray(n_vector_populations)).sum()/numpy.asarray(n_vector_populations).sum()
        if self.log_level < 16:
            print(f'{mpi_rank:d} : z_criterion = {load_balance_z_criterion:f}\tnvector.z = {n_vector_averages[mpi_rank]:f}\t{n_vector_populations[mpi_rank]:d} facets')
            print('weighted average:', n_vector_global_average)
        sys.stdout.flush()
        return n_vector_global_average

    def flat_load_optimization(self):
        if (self.loadweight != 0.) or (self.loadweight_offset != 0.):
            mid, f_mid, n_iter = search_by_dichotomy(
                function=self.test_load_z_criterion,
                left_limit=self.load_balance_z_criterion_min,
                right_limit=self.load_balance_z_criterion_max,
                max_iterations=self.max_iterations,
                log_level=self.log_level,
                search_for="max"
            )
        else:
            mid, f_mid, n_iter = search_by_dichotomy(
                function=self.test_load_z_criterion,
                left_limit=0,
                right_limit=0,
                max_iterations=1,
                log_level=self.log_level,
                search_for="max"
            )
        self.load_balance_z_criterion = mid
        self.load_balance_n_vector = f_mid
        if self.log_level < 30:
            print(f"\n>> Optimum found for z_criterion = {mid:f}, \tn_vector = {f_mid:f}, \tafter {n_iter:d} iterations        ")

    # work in progress......
    def vacuum_load_application(self):
        if self.log_level < 30:
            print(">> Applying and optimizing surface pressure (vacuum)...")
        self.update_cells_selection(None)
        self.stabilize_load_application_surface()

    def load_application_and_optimization(self):
        if self.use_vacuum:
            self.vacuum_load_application()
        else:
            self.flat_load_optimization()

    ##############################################################################
    # From here, the main problem has been solved, and we are formating results
    # and carrying out "post" computation
    ##############################################################################

    def post_compute_displacement_magnitude(self):
        if self.log_level < 30:
            print(">> Post-computing displacement magnitude...")
        # Compute magnitude of displacement
        u_magnitude = fenics.sqrt(fenics.dot(self.u_computed, self.u_computed))
        u_magnitude = fenics.project(u_magnitude, self.PointsFuncSpace)
        # #fenics.plot(u_magnitude, 'Displacement magnitude')
        # print maximum and minimum of the points displacement
        self.min_displacement_magnitude = numpy.asarray(u_magnitude.vector()[:])[~numpy.isnan(numpy.asarray(u_magnitude.vector()[:]))].min()
        self.max_displacement_magnitude = numpy.asarray(u_magnitude.vector()[:])[~numpy.isnan(numpy.asarray(u_magnitude.vector()[:]))].max()
        top_point_x, top_point_y, top_point_z = self.cells_midpoint_coordinates[self.cells_midpoint_coordinates[:, 2].argmax()]
        self.top_point = Point(x=top_point_x, y=top_point_y, z=top_point_z)
        self.top_point_displacement = self.top_point.get(self, self.u_computed)[2]

    def post_compute_internal_pressure(self):
        if self.log_level < 30:
            print(">> Post-computing internal pressure...")
        # Compute stress components
        # hydrostatic stress is the isotropic component of the stress tensor
        # it is the component responsible for any change in volume
        # hydstress is a tensor of shape (3, 3)
        hydstress = (1./3)*fenics.tr(sigma(u=self.u_computed, lambda_=self.lambda_, mu=self.mu, d=3))*fenics.Identity(3)
        # deviatoric stress is the component of the stress that is non-isotropic
        # it results in deformations without any change in volume
        # (but could it make a capillary vessel collapse ?)
        # (However, the pressure sensors in the silicone model can only detect volume changes,
        # so hydrostatic pressure is our main target here)
        #  #devstress = sigma(u) - hydstress
        # pressure (positive) is opposite to the hydrostatic stress
        # (that is negative if it causes a volume reduction, i.e. applies pressure)
        pressure_tensor = - hydstress  # in Pa, normally, if all other units are SI units

        # using the same variable names, but projecting over the Cells Function Space
        # Note that the pressure was calculated using point-wise vectors
        # But might have more meaning if expressed over volumes (cells)
        pressure_tensor = fenics.project(pressure_tensor, self.CellsTensorFuncSpace)
        #  #devstress = fenics.project(devstress, CellsTensorFuncSpace)

        # Since the pressure is isotropic, only 1 value per cell is needed, therefore,
        # define a new function over the Cells Function Space to take the value of the pressure
        self.pressure = fenics.Function(self.CellsFuncSpace)
        # assign the first value of the pressure tensor (0, 0) to the pressure function
        # (note: the pressure value can be found on the diagonal of the tensor)
        # the "sub" values of a (3, 3) tensor are counted from 0 to 8; 0, 4 and 8 being on the diagonal
        fenics.assign(self.pressure, pressure_tensor.sub(0))

        # projecting into the (continuous) points function space seems to work just fine
        # but values obtained in Paraview by coordinates are 0 when not coincident
        # with a mesh point, it seems...
        self.pressure = fenics.project(self.pressure, self.PointsFuncSpace)
        # then again, project on the appropriate function space for operability
        self.pressure = fenics.project(self.pressure, self.CellsFuncSpace)
        # scaling of pressure from Pascal to mmHg, for comparison purposes
        self.pressure_mmHg = (1./133.322)*self.pressure
        # It seems to be required to project pressure_mmHg again, for it to be a full UFL object
        self.pressure_mmHg = fenics.project(self.pressure_mmHg, self.CellsFuncSpace)

    def get_sensors_values(self, function=None, offset_angle_degree=0, offset_height=0):
        if (offset_angle_degree==0) and (offset_height==0):
            sensors = default_sensors
        else:
            sensors = Sensors(offset_angle_degree=offset_angle_degree, offset_height=offset_height)

        if function is None:
            function = self.pressure_mmHg

        try:
            sensors.get(simulation=self, function=function)
        except:
            print("!! One or more sensor(s) appear(s) to be outside of the deformed mesh.")

    def print_post_computation_details(self):
        print("\n##################################################################")
        print("Results:")
        if not self.use_vacuum:
            if (self.loadweight != 0) or (self.loadweight_offset != 0):
                print(f'z_criterion value : \t\t{self.load_balance_z_criterion:f} m')
            print(f'maximum displacement magnitude: \t{self.max_displacement_magnitude:f} m')
            print(f'top point vertical displacement: \t{self.top_point_displacement:f} m')
        if (self.loadweight != 0) or (self.loadweight_offset != 0):
            print(f'number of cells bearing load: \t{self.selected_cells.size:d}')
            print(f'estimated surface area: \t{self.load_application_surface:f} m^2')
        print(f'volume where P >= 1 mmHg: \t{self.pressure_mmHg.vector()[self.pressure_mmHg.vector()[:] >= 1.0].nonzero()[0].size / self.pressure_mmHg.vector()[:].size * 100 :f} %')
        print(f'volume where P >= 5 mmHg: \t{self.pressure_mmHg.vector()[self.pressure_mmHg.vector()[:] >= 5.0].nonzero()[0].size / self.pressure_mmHg.vector()[:].size * 100 :f} %')
        print(f'volume where P >= 10 mmHg: \t{self.pressure_mmHg.vector()[self.pressure_mmHg.vector()[:] >= 10.0].nonzero()[0].size / self.pressure_mmHg.vector()[:].size * 100 :f} %')
        print(f'volume where P >= 20 mmHg: \t{self.pressure_mmHg.vector()[self.pressure_mmHg.vector()[:] >= 20.0].nonzero()[0].size / self.pressure_mmHg.vector()[:].size * 100 :f} %')
        print("Sensors (mmHg):")
        self.get_sensors_values()
        print("##################################################################\n\n")

    def save_results_to_files(self, file_suffix=''):
        if self.log_level < 30:
            print(">> Saving results to files...")
        # Save solution to file in VTK format
        fenics.File(str('elasticity/displacement' + file_suffix + '.pvd')) << self.u_computed

        # #fenics.File('elasticity/magnitude.pvd') << u_magnitude

        # move the mesh according to the result of the calculation
        # disable to get the original mesh instead of the deformed one
        self.apply_mesh_deformation()

        # save calculated pressure in VTK format (deformed mesh)
        fenics.File(str('elasticity/pressure' + file_suffix + '.pvd')) << self.pressure
        fenics.File(str('elasticity/pressure_mmHg' + file_suffix + '.pvd')) << self.pressure_mmHg
        # #fenics.File('elasticity/deviatoricstress.pvd') << devstress
        fenics.File(str('elasticity/surface_load' + file_suffix + '.pvd')) << fenics.project(ufl.as_vector(self.Ps), self.CellsVectorFuncSpace)
        fenics.File(str('elasticity/total_load' + file_suffix + '.pvd')) << fenics.project(ufl.as_vector(ufl.as_vector(self.Ps)+ufl.as_vector(self.fv)), self.CellsVectorFuncSpace)

    def hold_plot(self):
        # Hold plot in the web browser if needed
        if (self.plot_level >= 1):
            plt.show()

    def post_computation(self, save_to_files=True, file_suffix='', show_summary=True):
        self.get_moved_points_coordinates()
        self.post_compute_displacement_magnitude()
        self.post_compute_internal_pressure()
        if show_summary:
            self.print_post_computation_details()
        if save_to_files:
            self.save_results_to_files(file_suffix=file_suffix)
        self.hold_plot()
        if self.log_level < 30:
            print("")

    # simplified call for a full computation + post-computation
    def compute(self, save_to_files=True, file_suffix='', keep_mesh_deformation=True, show_summary=True):
        self.load_application_and_optimization()
        self.post_computation(save_to_files=save_to_files, file_suffix=file_suffix, show_summary=show_summary)
        if not keep_mesh_deformation:
            self.undo_mesh_deformation()

    # get the closest cell (id number) from a given coordinate
    def closest_cell(self, point_coordinates):
        return vector_norm(self.cells_midpoint_coordinates - point_coordinates).argmin()

    # get the closest cell (id number) from a given coordinate
    # this time, in the mesh after deformation
    def closest_moved_cell(self, point_coordinates):
        return vector_norm(self.moved_cells_midpoint_coordinates - point_coordinates).argmin()

    # get the pressure of a point (coordinates) from the closest cell method
    def pressure_mmHg_of_point(self, point_coordinates):
        return self.pressure_mmHg.vector()[self.closest_moved_cell(point_coordinates)]

    def __init__(
        self,
        loadweight=loadweight_default,
        loadweight_offset=loadweight_offset_default,
        quarter_mesh=quarter_mesh_default,
        log_level=log_level_default,
        plot_level=plot_level_default,
        use_vacuum=use_vacuum_default,
        solver=solver_default,
        g=None,
        no_init=False,
        mesh_file=None
    ):
        self.set_instance_constants(
            loadweight=loadweight,
            loadweight_offset=loadweight_offset,
            quarter_mesh=quarter_mesh,
            log_level=log_level,
            plot_level=plot_level,
            use_vacuum=use_vacuum,
            g=g,
            solver=solver,
            mesh_file=mesh_file
        )
        if not no_init:
            self.problem_definition_and_initialization()
