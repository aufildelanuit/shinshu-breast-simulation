"""
Inspired from FEniCS tutorial demo program: Linear elastic problem.

Main equation for the linear elastic problem:
  -div(sigma(u)) = f

Adapted to the needs of a research topic on breast deformation.
OPOLKA Yohann, Shinshu University, Koseki Lab., 2020.02.10
Latest revision: 2020.02.14
"""


from simulation import New_Simulation
import argparse


# Initialize the argument parser with a description of what the program does
desc = 'Computes the elastic deformation of a silicone breast model under load (or own weight if load not specified)'
parser = argparse.ArgumentParser(description=desc)
# Set possible arguments and their default values
# Note that nargs='?' means 0 or 1 argument of this type can be specified
# const is the value taken when the argument name is given alone (ex: '--load' instead of '--load=0.400' or '--load 0.400')
# default is the value taken when the argument is not specified at all
parser.add_argument(
    '--file-suffix', '--fs',
    type=str,
    nargs='?',
    help='suffix added to the name of the data files created during the post-computation phase',
    const='',
    default=''
)
parser.add_argument(
    '-l', '--load',
    type=float,
    nargs='?',
    help='weight of the applied load in kg, 0.400 kg if given without any value, no load if unspecified',
    const=0.400,
    default=0
)
parser.add_argument(
    '--load-offset', '--lo',
    type=float,
    nargs='?',
    help='constant weight offset in kg, ex: weight of the platform on which the load is placed, 0.000 if unspecified',
    const=0.030,
    default=0.030
)
parser.add_argument(
    '-m', '--mesh-file',
    type=str, nargs='?',
    help='specify a mesh file to use for the simulation',
    const=None,
    default=None
)
parser.add_argument(
    '-p', '--plot-level',
    type=int,
    nargs='?',
    help='int value: No_plot=0, Plot_final_result=1, Plot_every_step=2',
    const=1,
    default=0
)
parser.add_argument(
    '-q', '--quarter-mesh',
    type=bool,
    nargs='?',
    help='use only a quarter of an hemisphere for the simulation',
    const=True,
    default=False
)
parser.add_argument(
    '--solver',
    '--model',
    type=str,
    nargs='?',
    help='mathematical model: linear, neo-hookean, yeoh',
    const="linear",
    default="linear"
)
parser.add_argument(
    '-v', '--log-level',
    type=int,
    nargs='?',
    help='int value: DBG=10, TRACE=13, PROGRESS=16, INFO=20, WARNING=30, ERROR=40, CRITICAL=50',
    const=16,
    default=30
)
parser.add_argument(
    '--vacuum',
    type=bool,
    nargs='?',
    help='use vacuum to generate surface pressure instead of a flat load',
    const=True,
    default=False
)
# Actually parse arguments
args = parser.parse_args()


simulation = New_Simulation(
    loadweight=args.load,
    loadweight_offset=args.load_offset,
    quarter_mesh=args.quarter_mesh,
    log_level=args.log_level,
    plot_level=args.plot_level,
    use_vacuum=args.vacuum,
    solver=args.solver,
    mesh_file=args.mesh_file
)

if args.file_suffix == '':
    if args.quarter_mesh:
        args.file_suffix = str('_q_offset' + str(int(1000 * args.load_offset)) + '_load' + str(int(1000 * args.load)))
    else:
        args.file_suffix = str('_f_offset' + str(int(1000 * args.load_offset)) + '_load' + str(int(1000 * args.load)))
# #simulation.problem_definition_and_initialization()
# #exit()
simulation.compute(file_suffix=args.file_suffix)
