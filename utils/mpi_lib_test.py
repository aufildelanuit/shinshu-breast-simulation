#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# #from mpi4py import MPI
import fenics


class testclass:
    def testfunction(self):
        self.comm = fenics.MPI.comm_world

        size = self.comm.Get_size()
        rank = self.comm.Get_rank()

        print("process %d / %d " % (rank, size))
