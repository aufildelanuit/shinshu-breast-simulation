#!/bin/bash

# quarter, no offset, 0.000kg
python3 simulation_main_modular.py -q --load-offset 0 -l 0

# quarter, no offset, 0.200kg
python3 simulation_main_modular.py -q --load-offset 0 -l 0.200

# quarter, no offset, 0.400kg
python3 simulation_main_modular.py -q --load-offset 0 -l 0.400

# quarter, no offset, 0.600kg
python3 simulation_main_modular.py -q --load-offset 0 -l 0.600

# full, no offset, 0.000kg
python3 simulation_main_modular.py --load-offset 0 -l 0

# full, no offset, 0.200kg
python3 simulation_main_modular.py --load-offset 0 -l 0.200

# full, no offset, 0.400kg
python3 simulation_main_modular.py --load-offset 0 -l 0.400

# full, no offset, 0.600kg
python3 simulation_main_modular.py --load-offset 0 -l 0.600

# quarter, with offset 0.030, 0.000kg
python3 simulation_main_modular.py -q -l 0

# quarter, with offset 0.030, 0.200kg
python3 simulation_main_modular.py -q -l 0.200

# quarter, with offset 0.030, 0.400kg
python3 simulation_main_modular.py -q -l 0.400

# quarter, with offset 0.030, 0.600kg
python3 simulation_main_modular.py -q -l 0.600

# full, with offset 0.030, 0.000kg
python3 simulation_main_modular.py -l 0

# full, with offset 0.030, 0.200kg
python3 simulation_main_modular.py -l 0.200

# full, with offset 0.030, 0.400kg
python3 simulation_main_modular.py -l 0.400

# full, with offset 0.030, 0.600kg
python3 simulation_main_modular.py -l 0.600
