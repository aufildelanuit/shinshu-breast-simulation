#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
import sys
import subprocess
import pip


def install(*arguments):
    subprocess.check_call([sys.executable, "-m", "pip", "install", *arguments])


# Install meshio and h5py
if __name__ == '__main__':
    if os.geteuid() == 0:
        print("Calling pip with root privilege for installing packages...")
        install('meshio')
        install('cython')
        install('--no-binary', 'h5py', 'h5py')
        install('pytz')
        install('tqdm')
        install('termcolor')
    else:
        print("Root privilege needed for installing python modules at the system scale,\nrestarting with sudo -H ...")
        subprocess.call(['sudo', '-H', 'python3', *sys.argv])
        sys.exit()
