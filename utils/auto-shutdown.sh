#!/bin/bash

basedir="$(echo "$0" | sed 's/\/.*$//')"

echo "" > "${basedir}/signal"

while true; do
	signal="$(cat "${basedir}/signal")"
	if [[ "${signal}" == "OK" ]]
	then
		break
	else
		env sleep 5
	fi
done
systemctl poweroff

