# Source Code for the Breast deformation simulation project using FEniCS

## Introduction

In this repository are stored the main programs written and used for the simulation of large deformations of the breast.

The main program, "simulation.py", is intended to be used as a library. 

A simple usage example can be found in "simulation_main_modular.py".

"colormaps.py" defines color maps to be used in the picture generation in "simulation_pressure_application_pattern.py".

The "utils" directories contains other tools, such as a script for configuring a FEniCS (Docker) container by installing some extra python libraries.

The "generated" directory is intended to contain files generated during the translation of the ".msh" meshes into other formats used by FEniCS.

The "elasticity" directory is originaly used to contain generated ".pvd" files that can be used to visualize the results in Paraview.

Finally, the "evolution" directory is intended to contain files generated and used by the genetic algorithm used in "simulation_pressure_application_pattern.py".

Most of the programs made for this project can accept command line arguments. See the help messahe of each program (invoked with -h or --help) for more details.



## Install instructions

In order to run fenics in a container, Docker (or alternatives like Podman) should be installed beforehand.

Instructions for installing Docker can be found on [Docker's website](https://docs.docker.com/get-started/) and in the [FEniCS online manual](https://fenics.readthedocs.io/projects/containers/en/latest/introduction.html).

From that point, install a FEniCS container in Docker with:
```bash
docker pull quay.io/fenicsproject/stable:latest
```

Download or clone the current project, then initialize the FEniCS container and (permanently) share the current directory with it:
```bash
docker run -ti -v $(pwd):/home/fenics/shared:z --name fenics quay.io/fenicsproject/stable
```

An alternative using podman is:
```bash
podman run -it --name fenics --pod new:fenics_pod -p 127.0.0.1:8000:8000 -v $(pwd):/home/fenics/shared:z -w /home/fenics/shared --security-opt label=disable quay.io/fenicsproject/stable:current
```

In subsequent launches, the container can be opened with:
```bash
docker start -a fenics
```
 
Or using podman:
```bash
podman start -a fenics
```

To stop the container, run:
```bash
docker stop fenics
```

```bash
podman stop fenics
```

Once inside the container, run the setup script using:
```bash
python3 utils/configure_fenics_container.py
```

Simulations can be started using:
```bash
python3 [-i] <simulation_file_name.py> [<arguments..>]
```

In case of problems with file permissions (files can't be written in the shared directory), try running as root inside the container:
```bash
sudo su
```

The current instance of the container can be removed with:
```bash
docker container rm fenics
```

```bash
podman container rm fenics
```

Replace "container" with "image" to remove the base image of the container (the image will have to be downloaded again to spawn new containers of the same kind.)
