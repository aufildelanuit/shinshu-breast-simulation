"""
Inspired from FEniCS tutorial demo program: Linear elastic problem.

Main equation for the linear elastic problem:
  -div(sigma(u)) = f

Adapted to the needs of a research topic on breast deformation.
OPOLKA Yohann, Shinshu University, Koseki Lab., 2020.03.06
Latest revision: 2020.04.02
"""


from simulation import New_Simulation
from simulation import Sensors
import numpy
import csv
from datetime import datetime
import pytz  # requires 'pip install pytz --user'
import os
import argparse


# Initialize the argument parser with a description of what the program does
desc = 'Computes the elastic deformation of a silicone breast model under several load conditions'
parser = argparse.ArgumentParser(description=desc)
# Set possible arguments and their default values
# Note that nargs='?' means 0 or 1 argument of this type can be specified
# const is the value taken when the argument name is given alone (ex: '--load' instead of '--load=0.400' or '--load 0.400')
# default is the value taken when the argument is not specified at all
parser.add_argument(
    '--dry-run',
    type=bool,
    nargs='?',
    help='if specified, skip main computation (for test purposes)',
    const=True,
    default=False
)
parser.add_argument(
    '--load-lower-bound',
    '--llb',
    type=float,
    nargs='?',
    help='minimum applied load in kg, 0 kg if not specified',
    const=0.,
    default=0.
)
parser.add_argument(
    '--load-upper-bound',
    '--lub',
    type=float,
    nargs='?',
    help='maximum applied load in kg, 1 kg if not specified',
    const=1.,
    default=1.
)
parser.add_argument(
    '--load-step',
    '--ls',
    type=float,
    nargs='?',
    help='step for the applied load in kg, 0.020 kg if not specified',
    const=0.020,
    default=0.020
)
parser.add_argument(
    '--offset-height-lower-bound',
    '--ohlb',
    type=float,
    nargs='?',
    help='minimum height offset for the sensors in m, -0.015 m if not specified',
    const=-0.015,
    default=-0.015
)
parser.add_argument(
    '--offset-height-upper-bound',
    '--ohub',
    type=float,
    nargs='?',
    help='maximum height offset for the sensors in m, +0.005 m if not specified',
    const=0.005,
    default=0.005
)
parser.add_argument(
    '--offset-height-step',
    '--ohs',
    type=float,
    nargs='?',
    help='step for the height offest tests in m, 0.001 m if not specified',
    const=0.001,
    default=0.001
)
parser.add_argument(
    '--offset-angle-lower-bound',
    '--oalb',
    type=float,
    nargs='?',
    help='minimum angle offset for the sensors position in degrees, 0 if not specified',
    const=0,
    default=0
)
parser.add_argument(
    '--offset-angle-upper-bound',
    '--oaub',
    type=float,
    nargs='?',
    help='maximum angle offset for the sensors position in degrees, 90 if not specified',
    const=90,
    default=90
)
parser.add_argument(
    '--offset-angle-step',
    '--oas',
    type=float,
    nargs='?',
    help='step for the angle offset tests in degrees, 15 if not specified',
    const=15,
    default=15
)
parser.add_argument(
    '-q', '--quarter-mesh',
    type=bool,
    nargs='?',
    help='simulate for a quarter of an hemisphere',
    const=True,
    default=False
)
parser.add_argument(
    '-f', '--full-mesh',
    type=bool,
    nargs='?',
    help='simulate for a full hemisphere',
    const=True,
    default=False
)
parser.add_argument(
    '-s', '--save-to-file',
    type=bool,
    nargs='?',
    help='if True (or given without argument): save results to csv file',
    const=True,
    default=False
)
parser.add_argument(
    '-v', '--log-level',
    type=int,
    nargs='?',
    help='int value: DBG=10, TRACE=13, PROGRESS=16, INFO=20, WARNING=30, ERROR=40, CRITICAL=50',
    const=16,
    default=30
)
# Actually parse arguments
args = parser.parse_args()


# Setting up the timezone and other filename initialization
timezone_japan = pytz.timezone('Asia/Tokyo')
# the following should return a string of the form YYYYmmdd-HHMM
current_time_string = datetime.now(timezone_japan).strftime("%Y%m%d-%H%M")
data_filename_prefix = "data_"
conditions_filename_prefix = "conditions_"
file_suffix_and_extension = ".csv"
relative_folder_path = "measurements"


# Data object, can contain just anything
class Data:
    pass


# Subclass of the Sensors object defined in simulation
# Inherits all the defined sensors, but slightly reformats
# the "get" method for readability
class Sensors_Extra(Sensors):
    def __init__(self, offset_angle_degree=0, offset_height=0):
        super().__init__(offset_angle_degree=offset_angle_degree, offset_height=offset_height)

    def get(self, simulation, function=None):
        simulation_mesh_deformation_undone = False
        sensors_list = self.auto_sensors_list(simulation)

        if simulation.mesh_is_deformed:
            simulation.undo_mesh_deformation()
            simulation_mesh_deformation_undone = True

        if function is None:
            function = simulation.pressure_mmHg

        print(f"loadweight:\t{simulation.loadweight:f} kg")
        print(f"offset angle:\t{self.offset_angle_degree:f} °")
        print(f"offset height:\t{self.offset_height:f} m")
        for i in sensors_list:
            try:
                sensor_value = i.get(simulation, function)
            except:
                sensor_value = numpy.nan
            if not numpy.isnan(sensor_value):
                print(f"{i.name:s}:\t{sensor_value:f}")
            else:
                print(f"{i.name:s}:\tNaN")
        print("")

        if simulation_mesh_deformation_undone:
            simulation.apply_mesh_deformation()


ongoing_simulations = []

if args.quarter_mesh:
    simulation_qm = New_Simulation(
        loadweight=args.load_lower_bound,
        quarter_mesh=True,
        log_level=args.log_level,
        plot_level=0,
        use_vacuum=False
    )
    ongoing_simulations.append(simulation_qm)
    if args.log_level < 30:
        # only needed if both meshes are tested
        if args.full_mesh:
            print(">> Done with quarter mesh initialization.\n\n")

if args.full_mesh:
    simulation_fm = New_Simulation(
        loadweight=args.load_lower_bound,
        quarter_mesh=False,
        log_level=args.log_level,
        plot_level=0,
        use_vacuum=False
    )
    ongoing_simulations.append(simulation_fm)
    if args.log_level < 30:
        # only needed if both meshes are tested
        if args.quarter_mesh:
            print(">> Done with full mesh initialization.\n\n")

sensors = Sensors_Extra()


def prepare_clean_temp_data_lists_1(simulation, sensors):
    sensors.data.temp_conditions_1 = []
    for s in sensors.auto_sensors_list(simulation):
        s.temp_data_1 = []


def prepare_clean_temp_data_lists_2(simulation, sensors):
    sensors.data.temp_conditions_2 = []
    for s in sensors.auto_sensors_list(simulation):
        s.temp_data_2 = []


def append_raw_data_to_temp_list_2(simulation, sensors):
    # in the Data's conditions dictionary, conditions are in order:
    # loadweight, offset_angle, offset_height, n_vector
    # the same order is kept to avoid confusion
    # the variables should be defined by the time this function is called
    sensors.data.temp_conditions_2.append([loadweight, offset_angle_degree, offset_height, simulation.load_balance_n_vector, simulation.top_point_displacement])
    if not args.dry_run:
        for s in sensors.auto_sensors_list(simulation):
            s.temp_data_2.append(s.get(simulation))


def append_temp_list_2_to_temp_list_1(simulation, sensors):
    sensors.data.temp_conditions_1.append(sensors.data.temp_conditions_2.copy())
    for s in sensors.auto_sensors_list(simulation):
        s.temp_data_1.append(s.temp_data_2.copy())


def append_temp_list_1_to_sensor_data(simulation, sensors):
    # in the Data's conditions dictionary, conditions are in order:
    # loadweight, offset_angle, offset_height, n_vector
    # in the temporary list, they are indexed in order from 0 to 2
    index = 0
    for condition in sensors.data.auto_conditions(simulation).keys():
        sensors.data.auto_conditions(simulation)[condition].append(
            (numpy.asarray(sensors.data.temp_conditions_1)[:, :, index]).astype(float).tolist()
        )
        index += 1
    for s in sensors.auto_sensors_list(simulation):
        s.data.append(s.temp_data_1.copy())


def cleanup_temporary_lists(simulation, sensors):
    del(sensors.data.temp_conditions_1)
    del(sensors.data.temp_conditions_2)
    for s in sensors.auto_sensors_list(simulation):
        del(s.temp_data_1)
        del(s.temp_data_2)


def convert_data_to_clean_numpy_array(simulation, sensors):
    for condition in sensors.data.auto_conditions(simulation).keys():
        # convert to a numpy array
        sensors.data.auto_conditions(simulation)[condition] = numpy.asarray(
            sensors.data.auto_conditions(simulation)[condition]
        )
    for s in sensors.auto_sensors_list(simulation):
        # convert to a numpy array
        s.data = numpy.asarray(
            s.data
        )
    sensors.update_data_dict_from_sensors()


def save_data_to_csv_file(simulation, sensors):
    if args.save_to_file:
        # create a folder with the given prefix where to save files
        dirname = relative_folder_path + '/' + current_time_string + '/'
        # the function should be called at much twice,
        # therefore, the output directory might already exist
        if not os.path.exists(dirname):
            try:
                # try to create it if it doesn't exist
                os.mkdir(dirname)
                if simulation.log_level < 30:
                    print(f">> Directory {dirname:s} created")
            except:
                print("!! Error encountered while trying to create a new directory")
                return
        else:
            # use the existing directory otherwise
            if simulation.log_level < 30:
                print(f">> Using existing directory: {dirname:s}")
        ##############################################################
        # first, save the data of each sensor in a separate csv file
        ##############################################################
        if simulation.log_level < 30:
            print(">> Saving data to individual files...")
        for s in sensors.auto_sensors_list(simulation):
            filename = dirname + data_filename_prefix + s.id + file_suffix_and_extension
            # 'a' stands for 'append'
            data_file = open(filename, 'a')
            # initialize a "writer" to write csv-formated data to the file
            writer = csv.writer(data_file)
            writer.writerows(s.data)
            data_file.close()
        ##############################################################
        # then, save the measurement conditions in one file
        ##############################################################
        if simulation.log_level < 30:
            print(">> Saving conditions to individual files...")
        for condition in sensors.data.auto_conditions(simulation).keys():
            filename = dirname + conditions_filename_prefix + condition + file_suffix_and_extension
            # 'a' stands for 'append'
            condition_file = open(filename, 'a')
            # initialize a "writer" to write csv-formated data to the file
            writer = csv.writer(condition_file)
            writer.writerows(sensors.data.auto_conditions(simulation)[condition])
            condition_file.close()


counter = 1
for sim in ongoing_simulations:
    if sim.log_level < 30:
        print(f"\n\n#> Starting loop for simulation {counter:d}, quarter_mesh={sim.quarter_mesh:d} ...")
    sim.prev_z_criterion = 0
    for loadweight in numpy.arange(args.load_lower_bound, args.load_upper_bound + args.load_step, args.load_step):
        sim.update_loadweight(loadweight)
        if not args.dry_run:
            if sim.load_balance_z_criterion != 0:
                # optimization of the optimum search, based on assumptions on the curve's shape (limited exponential)
                sim.load_balance_z_criterion_min = sim.load_balance_z_criterion
                sim.load_balance_z_criterion_max = (sim.load_balance_z_criterion + 3*(sim.load_balance_z_criterion - sim.prev_z_criterion))
            sim.prev_z_criterion = sim.load_balance_z_criterion
            sim.compute(save_to_files=False)
            ## for test purposes
            print(f"z_criterion = {sim.load_balance_z_criterion:f}")
        prepare_clean_temp_data_lists_1(sim, sensors)
        for offset_height in numpy.arange(args.offset_height_lower_bound, args.offset_height_upper_bound + args.offset_height_step, args.offset_height_step):
            sensors.change_offset_height(offset_height)
            prepare_clean_temp_data_lists_2(sim, sensors)
            for offset_angle_degree in numpy.arange(args.offset_angle_lower_bound, args.offset_angle_upper_bound + args.offset_angle_step, args.offset_angle_step):
                sensors.change_offset_angle(offset_angle_degree)
                if sim.log_level < 25:
                    if not args.dry_run:
                        sensors.get(sim)
                append_raw_data_to_temp_list_2(sim, sensors)
            append_temp_list_2_to_temp_list_1(sim, sensors)
        append_temp_list_1_to_sensor_data(sim, sensors)
        # towards a new iteration of the loadweight test loop
    cleanup_temporary_lists(sim, sensors)
    convert_data_to_clean_numpy_array(sim, sensors)
    # Since data is saved for each sensor separately,
    # and different sensors are used for a quarter mesh and a full mesh,
    # there should not be any duplicate...
    save_data_to_csv_file(sim, sensors)
    counter += 1
