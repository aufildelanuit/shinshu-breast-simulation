"""
Inspired from FEniCS tutorial demo program: Linear elastic problem.

Main equation for the linear elastic problem:
  -div(sigma(u)) = f

Adapted to the needs of a research topic on breast deformation.
OPOLKA Yohann, Shinshu University, Koseki Lab., 2020.06.04
Latest revision: 2020.07.10
"""


from simulation import New_Simulation, Point, vector_norm
import numpy
from PIL import Image, ImageDraw, ImageFont
import colormaps
from tqdm import tqdm
from termcolor import colored
import random
import pickle
import _thread
import argparse


# Initialize the argument parser with a description of what the program does
desc = 'Computes the elastic deformation of a silicone breast model under load (or own weight if load not specified)'
parser = argparse.ArgumentParser(description=desc)
# Set possible arguments and their default values
# Note that nargs='?' means 0 or 1 argument of this type can be specified
# const is the value taken when the argument name is given alone (ex: '--load' instead of '--load=0.400' or '--load 0.400')
# default is the value taken when the argument is not specified at all
parser.add_argument(
    '-c',
    '--resume',
    '--continue',
    type=bool,
    nargs='?',
    help='resume previously stopped process.',
    const=True,
    default=False
)
parser.add_argument(
    '--generation-limit', '--gl',
    type=int,
    nargs='?',
    help='limit for the number of generations in the genetic algorithm. 200 by default.',
    const=200,
    default=200
)
parser.add_argument(
    '-l', '--load',
    type=float,
    nargs='?',
    help='weight of the applied load in kg, 0.400 kg if given without any value, no load if unspecified',
    const=0.400,
    default=0.400
)
parser.add_argument(
    '--load-offset', '--lo',
    type=float,
    nargs='?',
    help='constant weight offset in kg, ex: weight of the platform on which the load is placed, 0.000 if unspecified',
    const=0.030,
    default=0.030
)
parser.add_argument(
    '-m', '--mesh-file',
    type=str, nargs='?',
    help='specify a mesh file to use for the simulation',
    const=None,
    default=None
)
parser.add_argument(
    '--phi-step-angle', '--psa',
    type=float,
    nargs='?',
    help='angle defining an isobar ring around the mesh for pressure application',
    const=5.0,
    default=5.0
)
parser.add_argument(
    '--phi-step-num', '--psn',
    type=int,
    nargs='?',
    help='number of isobar rings around the mesh for pressure application',
    const=15,
    default=0
)
parser.add_argument(
    '-p', '--plot-level',
    type=int,
    nargs='?',
    help='int value: No_plot=0, Plot_final_result=1, Plot_every_step=2',
    const=1,
    default=0
)
parser.add_argument(
    '-q', '--quarter-mesh',
    type=bool,
    nargs='?',
    help='use only a quarter of an hemisphere for the simulation',
    const=True,
    default=True
)
parser.add_argument(
    '--solver',
    '--model',
    type=str,
    nargs='?',
    help='mathematical model: linear, neo-hookean, yeoh',
    const="linear",
    default="linear"
)
parser.add_argument(
    '--theta-sampling', '--ts',
    type=int,
    nargs='?',
    help='number of slices taken inside the mesh when sampling internal pressure',
    const=11,
    default=11
)
parser.add_argument(
    '-v', '--log-level',
    type=int,
    nargs='?',
    help='int value: DBG=10, TRACE=13, PROGRESS=16, INFO=20, WARNING=30, ERROR=40, CRITICAL=50',
    const=16,
    default=30
)
parser.add_argument(
    '--vacuum',
    type=bool,
    nargs='?',
    help='use vacuum to generate surface pressure instead of a flat load',
    const=True,
    default=False
)
# Actually parse arguments
args = parser.parse_args()


# convert degree angles to rad
def degree_to_rad(angle_degree):
    return (angle_degree * numpy.pi / 180)


# convert rad angles to degree
def rad_to_degree(angle_rad):
    return (angle_rad * 180 / numpy.pi)


def to_x_y_theta(coordinates, from_spherical=False):
    coordinates = numpy.asarray(coordinates).T
    converted_coordinates = numpy.asarray(coordinates.copy(), dtype=float)
    # by default, assume carthesian (xyz) coordinates
    if not from_spherical:
        converted_coordinates[0] = numpy.sqrt(coordinates[0]**2 + coordinates[1]**2)
        converted_coordinates[1] = coordinates[2]
        converted_coordinates[2, (converted_coordinates[0] == 0)] = rad_to_degree(0)
        converted_coordinates[2, (converted_coordinates[0] != 0)] = rad_to_degree(numpy.arcsin(coordinates[1, (converted_coordinates[0] != 0)] / converted_coordinates[0, (converted_coordinates[0] != 0)]))
    # if coordinates were explicitely spherical
    else:
        converted_coordinates[0] = coordinates[0] * numpy.cos(degree_to_rad(coordinates[2]))
        converted_coordinates[1] = coordinates[0] * numpy.sin(degree_to_rad(coordinates[2]))
        converted_coordinates[2] = coordinates[1]
    return converted_coordinates.T


def to_x_y_z(coordinates, from_spherical=False):
    coordinates = numpy.asarray(coordinates).T
    converted_coordinates = numpy.asarray(coordinates.copy(), dtype=float)
    # by default, assume polar (xy_theta) coordinates
    if not from_spherical:
        converted_coordinates[0] = coordinates[0] * numpy.cos(degree_to_rad(coordinates[2]))
        converted_coordinates[1] = coordinates[0] * numpy.sin(degree_to_rad(coordinates[2]))
        converted_coordinates[2] = coordinates[1]
    # if coordinates were explicitely spherical
    else:
        converted_coordinates[0] = coordinates[0] * numpy.cos(degree_to_rad(coordinates[2])) * numpy.cos(degree_to_rad(coordinates[1]))
        converted_coordinates[1] = coordinates[0] * numpy.cos(degree_to_rad(coordinates[2])) * numpy.sin(degree_to_rad(coordinates[1]))
        converted_coordinates[2] = coordinates[0] * numpy.sin(degree_to_rad(coordinates[2]))
    return converted_coordinates.T


def to_r_theta_phi(coordinates, from_carthesian=False):
    coordinates = numpy.asarray(coordinates).T
    converted_coordinates = numpy.asarray(coordinates.copy(), dtype=float)
    # by default, assume polar (xy_theta) coordinates
    if not from_carthesian:
        converted_coordinates[0] = numpy.sqrt(coordinates[0]**2 + coordinates[1]**2)
        converted_coordinates[1] = coordinates[2]
        converted_coordinates[2, (converted_coordinates[0] == 0)] = rad_to_degree(0)
        converted_coordinates[2, (converted_coordinates[0] != 0)] = rad_to_degree(numpy.arcsin(coordinates[1, (converted_coordinates[0] != 0)] / converted_coordinates[0, (converted_coordinates[0] != 0)]))
    # if coordinates were explicitely carthesian (xyz)
    else:
        converted_coordinates[0] = numpy.sqrt(coordinates[0]**2 + coordinates[1]**2 + coordinates[2]**2)
        converted_coordinates[1, (numpy.sqrt(coordinates[0]**2 + coordinates[1]**2) == 0)] = rad_to_degree(0)
        converted_coordinates[1, (numpy.sqrt(coordinates[0]**2 + coordinates[1]**2) != 0)] = rad_to_degree(numpy.arcsin(coordinates[1, (numpy.sqrt(coordinates[0]**2 + coordinates[1]**2) != 0)] / numpy.sqrt(coordinates[0]**2 + coordinates[1]**2)[(numpy.sqrt(coordinates[0]**2 + coordinates[1]**2) != 0)]))
        converted_coordinates[2, (converted_coordinates[0] == 0)] = rad_to_degree(0)
        converted_coordinates[2, (converted_coordinates[0] != 0)] = rad_to_degree(numpy.arcsin(coordinates[2, (converted_coordinates[0] != 0)] / converted_coordinates[0, (converted_coordinates[0] != 0)]))
    return converted_coordinates.T


class New_Simulation(New_Simulation):
    def update_load_vector(self, Ps_dict=None):
        if self.log_level < 16:
            print(">>>> Updating load vector...")
        # defined as a force per exposed surface area
        # which actually makes it analogous to a pressure (seems accurate so far)
        if Ps_dict is None:
            self.Ps.z.vector()[:] = - self.cell_has_contact_with_load.vector()[:] * (self.load_scaling * (self.loadweight + self.loadweight_offset) * self.g) / (self.load_application_surface)
            if self.use_vacuum:
                # atmospheric pressure can be seen as directed along the normal to each facet
                self.Ps.x.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.x.vector()[:]
                self.Ps.y.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.y.vector()[:]
                self.Ps.z.vector()[:] = self.Ps.z.vector()[:] * self.external_facet_nvector.z.vector()[:]
        else:
            if ("default" in Ps_dict.keys()) and (len(numpy.asarray((Ps_dict["default"]))) == 3):
                # set the default value to all external cells, if a default value is provided
                # the format of a dictionary entry is: "<cell_num || "default">: (<x_val>, <y_val>, <z_val>)"
                self.Ps.x.vector()[:] = Ps_dict["default"][0]
                self.Ps.y.vector()[:] = Ps_dict["default"][1]
                self.Ps.z.vector()[:] = Ps_dict["default"][2]
            # now override the default value for the specified cells
            for cell in Ps_dict["by_cell"].keys():
                if len(numpy.asarray((Ps_dict["by_cell"][cell]))) == 3:
                    self.Ps.x.vector()[cell] = Ps_dict["by_cell"][cell][0]
                    self.Ps.y.vector()[cell] = Ps_dict["by_cell"][cell][1]
                    self.Ps.z.vector()[cell] = Ps_dict["by_cell"][cell][2]

    def update_cells_selection(self, load_balance_z_criterion):
        if self.log_level < 16:
            print(">>>> Updating cells selection...")
        # initialize or reinitialize the cells selection with a zero value (no cells selected)
        self.cell_has_contact_with_load.vector()[:] = 0.
        if self.use_vacuum:
            # if using vacuum, all the external cells are selected
            self.cell_has_contact_with_load.vector()[:] = self.cell_has_external_facet.vector()[:].copy()
        else:
            # select the cells on which the load is to be applied, by setting to 1
            # the "cell_has_contact_with_load" function value for the cells from the
            # highest midpoint coordinates and within the load_balance_z_criterion
            # (meaning: set cell_has_contact_with_load to 1 where the z coordinate
            # of the cell's midpoint is higher than the z coordinate of the highest
            # midpoint, minus the load_balance_z_criterion)
            self.cell_has_contact_with_load.vector()[(self.cells_midpoint_coordinates[:, 2] >= self.cells_midpoint_coordinates[self.cells_midpoint_coordinates[:, 2].argmax(), 2] - load_balance_z_criterion).nonzero()[0]] = 1
            # only keep the cells that are on the outermost layer
            self.cell_has_contact_with_load.vector()[:] *= self.cell_has_external_facet.vector()[:]
            # for clarity, save the selected cells' indexes in a separate array

        self.selected_cells = self.cell_has_contact_with_load.vector()[:].nonzero()[0]

    def stabilize_load_application_surface(self, Ps_dict=None):
        # Initialization
        self.update_exterior_facets_properties()
        self.update_load_application_surface()
        self.update_load_vector(Ps_dict)
        prev_load_application_surface = None
        # Stabilization loop
        # only needed if we are dealing with a load (force) and need to balance the application surface.
        # not needed if we are already dealing with a pressure
        while True:
            self.express_and_compute_variational_problem()
            self.apply_mesh_deformation()
            self.get_moved_points_coordinates()
            self.update_exterior_facets_properties()
            self.undo_mesh_deformation()
            self.update_load_application_surface()
            if self.log_level < 16:
                print("surface =", self.load_application_surface)
            if Ps_dict is None:
                self.update_load_vector(Ps_dict)
            else:
                break
            if prev_load_application_surface is not None:
                if numpy.abs(self.load_application_surface - prev_load_application_surface)/self.load_application_surface < 0.00001:
                    break
            prev_load_application_surface = self.load_application_surface

    def vacuum_load_application(self, Ps_dict=None):
        if self.log_level < 30:
            print(">> Applying and optimizing surface pressure (vacuum)...")
        self.update_cells_selection(None)
        self.stabilize_load_application_surface(Ps_dict)

    def load_application_and_optimization(self, Ps_dict=None):
        if self.use_vacuum:
            self.vacuum_load_application(Ps_dict)
        else:
            self.flat_load_optimization()

    # simplified call for a full computation + post-computation
    def compute(self, save_to_files=False, keep_mesh_deformation=False, show_summary=True, Ps_dict=None):
        self.load_application_and_optimization(Ps_dict)
        self.post_computation(save_to_files=save_to_files, show_summary=show_summary)
        if not keep_mesh_deformation:
            self.undo_mesh_deformation()
            self.update_exterior_facets_properties()


simulation = New_Simulation(
    loadweight=args.load,
    loadweight_offset=args.load_offset,
    quarter_mesh=args.quarter_mesh,
    log_level=args.log_level,
    plot_level=args.plot_level,
    use_vacuum=args.vacuum,
    solver=args.solver,
    mesh_file=args.mesh_file
)

# #simulation.problem_definition_and_initialization()
# #exit()
simulation.compute()


def sepia(image, k=1):
    # Load the image as an array so cv knows how to work with it
    img = numpy.array(image)
    if k > 0:
        filt = numpy.array([x for x in img])
        while k > 0:
            k_v = min(k, 1)
            # Apply a transformation where we multiply each pixel
            # rgb with the matrix transformation for the sepia
            #lmap = numpy.matrix(
            #    [[0.393 + 0.607 * (1 - k_v), 0.769 - 0.769 * (1 - k_v), 0.189 - 0.189 * (1 - k_v)],
            #     [0.349 - 0.349 * (1 - k_v), 0.686 + 0.314 * (1 - k_v), 0.168 - 0.168 * (1 - k_v)],
            #     [0.272 - 0.349 * (1 - k_v), 0.534 - 0.534 * (1 - k_v), 0.131 + 0.869 * (1 - k_v)]]
            #)
            lmap = numpy.matrix(
                [[0.393 + 0.607 * (1 - k_v), 0.769 - 0.769 * (1 - k_v), 0.189 - 0.189 * (1 - k_v)],
                 [0.349 - 0.349 * (1 - k_v), 0.686 + 0.314 * (1 - k_v), 0.168 - 0.168 * (1 - k_v)],
                 [0.272 - 0.349 * (1 - k_v), 0.534 - 0.534 * (1 - k_v), 0.131 + 0.869 * (1 - k_v)]]
            )
            filt = numpy.array([x * lmap.T for x in filt])
            # Check wich entries have a value greather than 255 and set it to 255
            filt[numpy.where(filt > 255)] = 255
            k = k - 1
        # Create an image from the array
        return Image.fromarray(filt.astype('uint8'))
    else:
        return image


def magma(image, cells_mask=None):
    img = numpy.asarray(image).mean(axis=2).astype(numpy.uint8)
    img = (colormaps.magma_colormap(img)[:, :, 0:3] * 255).astype(numpy.uint8)
    if cells_mask is not None:
        img = (img * cells_mask)
    return Image.fromarray(img.astype(numpy.uint8))


def inferno(image, cells_mask=None):
    img = numpy.asarray(image).mean(axis=2).astype(numpy.uint8)
    img = (colormaps.inferno_colormap(img)[:, :, 0:3] * 255).astype(numpy.uint8)
    if cells_mask is not None:
        img = (img * cells_mask)
    return Image.fromarray(img.astype(numpy.uint8))


def plasma(image, cells_mask=None):
    img = numpy.asarray(image).mean(axis=2).astype(numpy.uint8)
    img = (colormaps.plasma_colormap(img)[:, :, 0:3] * 255).astype(numpy.uint8)
    if cells_mask is not None:
        img = (img * cells_mask)
    return Image.fromarray(img.astype(numpy.uint8))


def viridis(image, cells_mask=None):
    img = numpy.asarray(image).mean(axis=2).astype(numpy.uint8)
    img = (colormaps.viridis_colormap(img)[:, :, 0:3] * 255).astype(numpy.uint8)
    if cells_mask is not None:
        img = (img * cells_mask)
    return Image.fromarray(img.astype(numpy.uint8))


def image_filter(image, filter='nofilter', k=1, cells_mask=None):
    if filter == 'nofilter':
        pass
    elif filter == 'sepia':
        image = sepia(image, k=k)
    else:
        try:
            filter_call = globals()[filter]
            image = filter_call(image, cells_mask)
        except:
            print(f"!! {filter} filter is not defined !!")
    return image


def cells_on_x_slice(x, filter_skin_cells=True):
    points_per_cell = cells_points_slice_coordinates.shape[1]
    if x == 0:
        cells = (cells_points_slice_coordinates[:, :, 0] > x)
    else:
        cells = (cells_points_slice_coordinates[:, :, 0] < x)
    cells = numpy.count_nonzero(cells, axis=1)
    cells = ((cells > 0) & (cells < points_per_cell)).nonzero()[0]
    # exclude skin cells
    if filter_skin_cells:
        cells = numpy.intersect1d(cells, non_skin_cells)
    return cells


def cells_on_y_slice(y, filter_skin_cells=True):
    points_per_cell = cells_points_slice_coordinates.shape[1]
    if y == 0:
        cells = (cells_points_slice_coordinates[:, :, 1] > y)
    else:
        cells = (cells_points_slice_coordinates[:, :, 1] < y)
    cells = numpy.count_nonzero(cells, axis=1)
    cells = ((cells > 0) & (cells < points_per_cell)).nonzero()[0]
    # exclude skin cells
    if filter_skin_cells:
        cells = numpy.intersect1d(cells, non_skin_cells)
    return cells


def cells_on_theta_slice(theta_degree, filter_skin_cells=True):
    points_per_cell = cells_points_slice_coordinates.shape[1]
    if theta_degree == 0:
        cells = (cells_points_slice_coordinates[:, :, 2] > theta_degree)
    else:
        cells = (cells_points_slice_coordinates[:, :, 2] < theta_degree)
    cells = numpy.count_nonzero(cells, axis=1)
    cells = ((cells > 0) & (cells < points_per_cell)).nonzero()[0]
    # exclude skin cells
    if filter_skin_cells:
        cells = numpy.intersect1d(cells, non_skin_cells)
    return cells


def cells_on_xy_circle(x, y, filter_skin_cells=True):
    # select cells according to the x coordinate
    points_per_cell = cells_points_slice_coordinates.shape[1]
    if x == 0:
        x_cells = (cells_points_slice_coordinates[:, :, 0] > x)
    else:
        x_cells = (cells_points_slice_coordinates[:, :, 0] < x)
    x_cells = numpy.count_nonzero(x_cells, axis=1)
    x_cells = ((x_cells > 0) & (x_cells < points_per_cell)).nonzero()[0]
    # now, amongst those cells, select on the y coordinate
    if y == 0:
        y_cells = (cells_points_slice_coordinates[x_cells, :, 1] > y)
    else:
        y_cells = (cells_points_slice_coordinates[x_cells, :, 1] < y)
    y_cells = numpy.count_nonzero(y_cells, axis=1)
    y_cells = ((y_cells > 0) & (y_cells < points_per_cell)).nonzero()[0]
    # get the final cells list
    cells = x_cells[y_cells]
    # exclude skin cells
    if filter_skin_cells:
        cells = numpy.intersect1d(cells, non_skin_cells)
    return cells


def cell_at_xytheta_point(x, y, theta_degree, filter_skin_cells=True, unique=True):
    # select cells according to the x coordinate
    points_per_cell = cells_points_slice_coordinates.shape[1]
    if x == 0:
        x_cells = (cells_points_slice_coordinates[:, :, 0] > x)
    else:
        x_cells = (cells_points_slice_coordinates[:, :, 0] < x)
    x_cells = numpy.count_nonzero(x_cells, axis=1)
    x_cells = ((x_cells > 0) & (x_cells < points_per_cell)).nonzero()[0]
    # now, amongst those cells, select on the y coordinate
    if y == 0:
        y_cells = (cells_points_slice_coordinates[x_cells, :, 1] > y)
    else:
        y_cells = (cells_points_slice_coordinates[x_cells, :, 1] < y)
    y_cells = numpy.count_nonzero(y_cells, axis=1)
    y_cells = ((y_cells > 0) & (y_cells < points_per_cell)).nonzero()[0]
    # get the xy cells list
    xy_cells = x_cells[y_cells]
    # select by theta value
    if theta_degree == 0:
        t_cells = (cells_points_slice_coordinates[xy_cells, :, 2] > theta_degree)
    else:
        t_cells = (cells_points_slice_coordinates[xy_cells, :, 2] < theta_degree)
    t_cells = numpy.count_nonzero(t_cells, axis=1)
    t_cells = ((t_cells > 0) & (t_cells < points_per_cell)).nonzero()[0]
    # get the xy cells list
    cells = xy_cells[t_cells]
    # exclude skin cells
    if filter_skin_cells:
        cells = numpy.intersect1d(cells, non_skin_cells)
    if cells.size > 1 and unique:
        cells = cells[(cells_midpoint_slice_coordinates[cells][:, 2] - theta_degree).argmin()]
    return cells


def Pa_to_mmHg(pressure_Pa):
    return 0.00750062 * pressure_Pa


def mmHg_to_Pa(pressure_mmHg):
    return pressure_mmHg / 0.00750062


cell_at_xytheta_point_vect = numpy.vectorize(cell_at_xytheta_point)

image_font = ImageFont.truetype("Ubuntu-R.ttf", size=16)

image = Image.new("RGB", (960, 640), "#000000")

bottom_margin = 10
top_margin = 10
left_margin = 10
right_margin = 10
side_length = 2/3*(image.size[1] - top_margin - bottom_margin)

default_filter = 'inferno'
sepia_coef = 1.2

theta_sampling = args.theta_sampling  # total number of theta samples
if args.phi_step_num != 0:
    phi_step_angle = (90 / args.phi_step_num)
else:
    phi_step_angle = args.phi_step_angle  # angle in degree of phi intervals

indicator_bottom_margin = int(10)
indicator_top_margin = int(10)
indicator_width = int(2*numpy.tan(((2*numpy.arctan(15/(2*423)))*(phi_step_angle))/(5*2))*(int(side_length)+indicator_bottom_margin))

legend_width = int(30)
legend_length = int(200)

model_picture = Image.new("RGB", (int(side_length)+2, int(side_length)+2), "#000000")
model_and_indicators_picture = Image.new("RGB", (int(image.size[1] - bottom_margin - top_margin), int(image.size[1] - bottom_margin - top_margin)), "#000000")

max_pressure_on_scale = 20  # in mmHg
min_pressure_on_scale = 0   # in mmHg

cells_points_slice_coordinates = to_x_y_theta(simulation.cells_points_coordinates)
cells_midpoint_slice_coordinates = to_x_y_theta(simulation.cells_midpoint_coordinates)
cells_points_polar_coordinates = to_r_theta_phi(simulation.cells_points_coordinates, from_carthesian=True)
cells_midpoint_polar_coordinates = to_r_theta_phi(simulation.cells_midpoint_coordinates, from_carthesian=True)
non_skin_cells = (simulation.subdomains.array()[:] > 0).nonzero()[0]

limit_dimension = (simulation.model_radius - simulation.skin_thickness)

bbt = simulation.mesh.bounding_box_tree()
total_num_of_cells_with_external_facets = simulation.cell_has_external_facet.vector()[:].nonzero()[0].size
## clear the picture, for tests
#image.paste((0, 0, 0), [0, 0, image.size[0], image.size[1]])
#model_picture.paste((0, 0, 0), [0, 0, model_picture.size[0], model_picture.size[1]])


def coordinates_mapping():
    print("\n>> mapping picture coordinates to mesh coordinates...")
    # we suppose that we will take 20 slices of theta
    global array_position_to_xytheta_coordinates_coef
    global array_position_to_slice_coordinates_map
    global array_position_to_slice_coordinates_map_individual
    global array_position_to_xyz_coordinates_map
    global array_position_to_cells_list_map
    global cells_mask
    global cells_with_external_facets_dict
    array_position_to_slice_coordinates_map_base = numpy.empty((model_picture.size[0], model_picture.size[1], theta_sampling), dtype=float)
    array_position_to_slice_coordinates_map_base[:, :, :] = numpy.nan
    # ##
    array_position_to_xytheta_coordinates_coef = numpy.asarray([[
        limit_dimension / (array_position_to_slice_coordinates_map_base.shape[0] - 1),
        limit_dimension / (array_position_to_slice_coordinates_map_base.shape[1] - 1),
        (90 - 0) / (array_position_to_slice_coordinates_map_base.shape[2] - 1)
    ]]).T
    # ##
    array_position_to_slice_coordinates_map_individual = numpy.asarray([
        numpy.repeat(numpy.arange(array_position_to_slice_coordinates_map_base.shape[0]), array_position_to_slice_coordinates_map_base.shape[1]*array_position_to_slice_coordinates_map_base.shape[2]),
        numpy.tile(numpy.repeat(numpy.arange(array_position_to_slice_coordinates_map_base.shape[1]), array_position_to_slice_coordinates_map_base.shape[2]), array_position_to_slice_coordinates_map_base.shape[0]),
        numpy.tile(numpy.arange(array_position_to_slice_coordinates_map_base.shape[2]), array_position_to_slice_coordinates_map_base.shape[0]*array_position_to_slice_coordinates_map_base.shape[1])
    ])*array_position_to_xytheta_coordinates_coef
    array_position_to_slice_coordinates_map_individual = numpy.reshape(array_position_to_slice_coordinates_map_individual, (3, model_picture.size[0], model_picture.size[1], theta_sampling))
    array_position_to_slice_coordinates_map = numpy.empty((model_picture.size[0], model_picture.size[1], theta_sampling, 3))
    array_position_to_slice_coordinates_map[:, :, :, 0] = array_position_to_slice_coordinates_map_individual[0]
    array_position_to_slice_coordinates_map[:, :, :, 1] = array_position_to_slice_coordinates_map_individual[1]
    array_position_to_slice_coordinates_map[:, :, :, 2] = array_position_to_slice_coordinates_map_individual[2]
    array_position_to_xyz_coordinates_map = to_x_y_z(array_position_to_slice_coordinates_map)
    cells_mask = (numpy.sqrt(array_position_to_xyz_coordinates_map.T[0]**2 + array_position_to_xyz_coordinates_map.T[1]**2 + array_position_to_xyz_coordinates_map.T[2]**2) < limit_dimension).T
    # ##
    array_position_to_cells_list_map = {}
    for ((p_x, p_y, p_th), _) in tqdm(numpy.ndenumerate(array_position_to_slice_coordinates_map_individual[0]), total=array_position_to_slice_coordinates_map_individual[0].size, desc="mapping", unit="pixels", leave=False):
        if p_x not in array_position_to_cells_list_map.keys():
            array_position_to_cells_list_map[p_x] = {}
        if p_y not in array_position_to_cells_list_map[p_x].keys():
            array_position_to_cells_list_map[p_x][p_y] = {}
        array_position_to_cells_list_map[p_x][p_y][p_th] = bbt.compute_collisions(Point(*array_position_to_xyz_coordinates_map[p_x, p_y, p_th]).fenics)
    # ##
    # simulation.cell_has_contact_with_load.vector()[:].nonzero()[0].size
    #cells_midpoint_polar_coordinates[simulation.cell_has_contact_with_load.vector()[:]==1][:, 2]
    cells_with_external_facets_dict = dict()
    for phi in numpy.arange(90-(phi_step_angle/2), 0, -phi_step_angle):
        cells_with_external_facets_dict[phi] = {
            "angle_min": phi-(phi_step_angle/2),
            "angle_max": phi+(phi_step_angle/2),
            "cells_list": (simulation.cell_has_external_facet.vector()[:]==1).nonzero()[0][
                                numpy.intersect1d(
                                    (cells_midpoint_polar_coordinates[simulation.cell_has_external_facet.vector()[:]==1][:, 2] <= phi+(phi_step_angle/2)).nonzero()[0],
                                    (cells_midpoint_polar_coordinates[simulation.cell_has_external_facet.vector()[:]==1][:, 2] >= phi-(phi_step_angle/2)).nonzero()[0]
                                )
                            ],
        }


def get_Ps_norm_on_external_facets(phi_deg):
    return vector_norm([numpy.asarray(simulation.Ps.x.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]]), numpy.asarray(simulation.Ps.y.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]]), numpy.asarray(simulation.Ps.z.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]])]).mean()


def array_to_picture_coordinates(coordinates, picture_size):
    coordinates = numpy.asarray(coordinates).T
    picture_size = numpy.asarray(picture_size)
    x = coordinates[0]
    y = coordinates[1]
    y = picture_size[1] - y
    return numpy.uint64(numpy.asarray([x, y]).T)


def closest_phi_slice(phi_deg):
    for phi_slice in cells_with_external_facets_dict.keys():
        if (phi_deg <= cells_with_external_facets_dict[phi_slice]["angle_max"]) and (phi_deg >= cells_with_external_facets_dict[phi_slice]["angle_min"]):
            return phi_slice


def average_Ps_vector(phi_deg, true_average=None, from_indicated_phi=False):
    if true_average is None:
        if simulation.use_vacuum:
            true_average = False
        else:
            true_average = True
    if from_indicated_phi:
        original_phi_deg = phi_deg
    if phi_deg not in cells_with_external_facets_dict.keys():
        phi_deg = closest_phi_slice(phi_deg)
    Ps_vects = to_r_theta_phi(
        numpy.asarray([
            simulation.Ps.x.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]],
            simulation.Ps.y.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]],
            simulation.Ps.z.vector()[cells_with_external_facets_dict[phi_deg]["cells_list"]]
        ]).T, from_carthesian=True
    )
    # if some of the selected cells have a null vector,
    # count them in the r average, but not for measuring the average phi and theta
    # (because null vectors shouldn't change the direction of the average)
    Ps_vect_av = numpy.asarray([
        Ps_vects.T[0].mean(),
        Ps_vects.T[1][Ps_vects.T[0].nonzero()[0]].mean() if Ps_vects.T[1][Ps_vects.T[0].nonzero()[0]].size > 0 else 0.,
        Ps_vects.T[2][Ps_vects.T[0].nonzero()[0]].mean() if Ps_vects.T[2][Ps_vects.T[0].nonzero()[0]].size > 0 else 0.
    ])
    # express the vector from the middle of the phi slice, and at theta = 45 deg,
    # unless specified to do otherwise
    # this corrects the aspect of the indicators when vacuum is used
    # (because in each slice, there are more cells bellow the center of the phi slice than upon it,
    # this induces a slight deviation : the phi of the average vector is different from the phi used
    # for representing the vector on the picture, hence the apparition of a slight deviation, generally to the right)
    if not true_average:
        Ps_vect_av[1] = 45.
        if from_indicated_phi:
            Ps_vect_av[2] = - original_phi_deg
        else:
            Ps_vect_av[2] = - phi_deg
    return to_x_y_z(Ps_vect_av, from_spherical=True)


def indicator_pixel_coordinates(phi_deg, indicator_bottom_margin=indicator_bottom_margin, indicator_top_margin=indicator_top_margin):
    base_point = numpy.round(
        (to_x_y_theta(
            [(model_picture.size[1] + indicator_bottom_margin) * array_position_to_xytheta_coordinates_coef[0], 45, phi_deg], from_spherical=True
        ) / array_position_to_xytheta_coordinates_coef.T)[0][0:2]
    )
    reversed_Ps_vector = numpy.round(
        to_x_y_theta(
            (-1) * average_Ps_vector(phi_deg)
        ) * (model_and_indicators_picture.size[1] - model_picture.size[1] - indicator_bottom_margin - indicator_top_margin) / 1500
    )[0:2]
    top_point = base_point + reversed_Ps_vector
    return array_to_picture_coordinates([base_point, top_point], model_and_indicators_picture.size).flatten().tolist()


# ##
def generate_picture(filename, filter=default_filter):
    if simulation.log_level < 30:
        print(">> sampling internal pressure in mesh and generating picture...")
    global image
    global model_picture
    global pressure_map
    global min_pressure_on_scale
    global max_pressure_on_scale
    global left_margin
    global right_margin
    global bottom_margin
    global top_margin
    # clear the existing pictures
    image.paste((0, 0, 0), [0, 0, image.size[0], image.size[1]])
    model_picture.paste((0, 0, 0), [0, 0, model_picture.size[0], model_picture.size[1]])
    model_and_indicators_picture.paste((0, 0, 0), [0, 0, model_and_indicators_picture.size[0], model_and_indicators_picture.size[1]])
    pressure_map = [
        (
            simulation.pressure_mmHg.vector()[array_position_to_cells_list_map[p_x][p_y][p_th]].mean() if cells_mask[p_x, p_y, p_th]==True else numpy.nan
        ) for ((p_x, p_y, p_th), _) in tqdm(numpy.ndenumerate(array_position_to_slice_coordinates_map_individual[0]), total=array_position_to_slice_coordinates_map_individual[0].size, desc="sampling", unit="pixels", leave=False)
    ]
    pressure_map = numpy.reshape(numpy.asarray(pressure_map), (model_picture.size[0], model_picture.size[1], theta_sampling))
    pressure_map = pressure_map.mean(axis=2)
    pressure_map[numpy.isnan(pressure_map)] = min_pressure_on_scale
    pressure_map[pressure_map < min_pressure_on_scale] = min_pressure_on_scale
    pressure_map[pressure_map > max_pressure_on_scale] = max_pressure_on_scale
    pixel_values_map = numpy.uint8(pressure_map / (max_pressure_on_scale - min_pressure_on_scale) * 255)
    model_picture = Image.fromarray(numpy.repeat(numpy.uint8(numpy.flip(pixel_values_map.T, axis=0))[:, :, None], 3, axis=2), mode="RGB")
    # create a mask for the color filter to be applied only "inside" the model on the picture
    model_pixels_mask = numpy.flip(numpy.tile(cells_mask[:, :, 0, None], 3), axis=0)
    # for increasing visibility of the differences
    model_picture = image_filter(model_picture, filter=filter, k=sepia_coef, cells_mask=model_pixels_mask)
    # drawing contour
    draw = ImageDraw.Draw(model_picture)
    draw.pieslice([(- model_picture.size[0] + 1), (0), (model_picture.size[0] - 1), (2 * model_picture.size[1] - 2)], -90, 0, outline="#FFFFFF")
    # pasting the model picture on the picture where the indicators will be added
    model_and_indicators_picture.paste(model_picture, [0, (model_and_indicators_picture.size[1] - model_picture.size[1]), (model_picture.size[1]), (model_and_indicators_picture.size[1])])
    # drawing the indicators
    if simulation.log_level < 30:
        print(">> generating external pressure indicators...")
    draw = ImageDraw.Draw(model_and_indicators_picture)
    for phi in cells_with_external_facets_dict.keys():
        #print(phi, get_Ps_norm_on_external_facets(phi), indicator_pixel_coordinates(phi))
        #draw.polygon()
        draw.line(indicator_pixel_coordinates(phi), fill="#FFFFFF", width=indicator_width)
    #
    # pasting on the larger, full image
    image.paste(model_and_indicators_picture, [left_margin, (image.size[1] - model_and_indicators_picture.size[1] - bottom_margin), (left_margin + model_and_indicators_picture.size[1]), (image.size[1] - bottom_margin)])
    # draw legends
    draw = ImageDraw.Draw(image)
    if simulation.log_level < 30:
        print(">> generating legends...")
    legend1_start_point_x = int(model_and_indicators_picture.size[0] + (image.size[0] - model_and_indicators_picture.size[0]) * 1/4)
    legend1_start_point_y = int(image.size[1] * 1/10)
    legend1_stop_point_x = int(legend1_start_point_x + legend_width)
    legend1_stop_point_y = int(legend1_start_point_y + legend_length)
    legend1 = Image.new("RGB", (legend_width + 1, legend_length + 1), "#000000")
    draw = ImageDraw.Draw(legend1)
    draw.rectangle([0, 0, legend_width, legend_length], outline="#FFFFFF")
    for y_i in range(1, legend_length):
        colour_i = numpy.uint8(numpy.round((legend_length - y_i - 1) / (legend_length - 2) * ((pressure_map.max() / (max_pressure_on_scale - min_pressure_on_scale) * 255) - (pressure_map.min() / (max_pressure_on_scale - min_pressure_on_scale) * 255)) + (pressure_map.min() / (max_pressure_on_scale - min_pressure_on_scale) * 255) ))
        #print(y_i, colour_i)
        draw.line([(1), (y_i), (legend_width - 1), (y_i)], fill=(colour_i, colour_i, colour_i))
    #
    # ##
    legend1 = image_filter(legend1, filter, sepia_coef)
    image.paste(legend1, [int(legend1_start_point_x), int(legend1_start_point_y), int(legend1_start_point_x + legend1.size[0]), int(legend1_start_point_y + legend1.size[1])])
    draw = ImageDraw.Draw(image)
    draw.text([int(legend1_start_point_x), int(legend1_start_point_y - 1.8 * (draw.textsize("Hydrostatic pressure (mmHg)", font=image_font)[1]))], "Hydrostatic pressure (mmHg)", font=image_font)
    even_tick = True
    for y_i in range(legend1_start_point_y, legend1_stop_point_y + 1, int(legend_length / 10)):
        tick_value = numpy.round((( 1 - ( (y_i - legend1_start_point_y) / legend_length ) ) * (pressure_map.max() - pressure_map.min()) + pressure_map.min()), 2)
        tick_text = str(tick_value)
        if even_tick:
            tick_size = 10
            even_tick = False
            draw.text([int(legend1_stop_point_x + 15), int(y_i - numpy.ceil(draw.textsize(tick_text, font=image_font)[1] / 2) - 2)], tick_text, font=image_font)
        else:
            tick_size = 5
            even_tick = True
        draw.line([int(legend1_stop_point_x), int(y_i), int(legend1_stop_point_x + tick_size), int(y_i)], fill="#FFFFFF")
    #
    legend2_start_point_x = int(model_and_indicators_picture.size[0] + (image.size[0] - model_and_indicators_picture.size[0]) * 1/4 + legend_width - 15)
    legend2_start_point_y = int(image.size[1] * 5.5/10)
    legend2_stop_point_x = int(legend2_start_point_x)
    legend2_stop_point_y = int(legend2_start_point_y + (model_and_indicators_picture.size[1] - model_picture.size[1] - indicator_bottom_margin - indicator_top_margin))
    draw.line([legend2_start_point_x, legend2_start_point_y, legend2_stop_point_x, legend2_stop_point_y], fill="#FFFFFF", width=15)
    draw.text([int(legend1_start_point_x), int(legend2_start_point_y - 1.8 * (draw.textsize("Applied pressure (mmHg)", font=image_font)[1]))], "Applied pressure (mmHg)", font=image_font)
    even_tick = True
    for y_i in range(legend2_start_point_y, legend2_stop_point_y + 1, int((legend2_stop_point_y - legend2_start_point_y) / 10)):
        tick_value = numpy.round((( 1 - ( (y_i - legend2_start_point_y) / (legend2_stop_point_y - legend2_start_point_y) ) ) * (Pa_to_mmHg(1500))), 2)
        tick_text = str(tick_value)
        if even_tick:
            tick_size = 10
            even_tick = False
            draw.text([int(legend2_stop_point_x + numpy.floor(indicator_width / 2) + 15), int(y_i - numpy.ceil(draw.textsize(tick_text, font=image_font)[1] / 2) - 2)], tick_text, font=image_font)
        else:
            tick_size = 5
            even_tick = True
        draw.line([int(legend2_stop_point_x + numpy.floor(indicator_width / 2)), int(y_i), int(legend2_stop_point_x + numpy.floor(indicator_width / 2) + tick_size), int(y_i)], fill="#FFFFFF")
    #
    draw.text([int(legend1_start_point_x), int(image.size[1] - 2 * draw.textsize(str(f"Average over {theta_sampling:d} sample slice(s)"), font=image_font)[1])], str(f"Average over {theta_sampling:d} sample slice(s)"), font=image_font)
    # saving to file
    image.save(filename)
    if simulation.log_level < 30:
        print("")


def Ps_dict_generator(vnorm_by_angle_dict):
    Ps_dict = {
        "default": (0., 0., 0.),
        "by_cell": {}
    }
    for phi_i in cells_with_external_facets_dict.keys():
        # distribute Ps along the normal vector of each cell, write the result in a dictionary
        for cell_num in cells_with_external_facets_dict[phi_i]["cells_list"]:
            Ps_dict["by_cell"][cell_num] = vnorm_by_angle_dict[phi_i] * (-1) * numpy.asarray([
                simulation.external_facet_nvector.x.vector()[cell_num],
                simulation.external_facet_nvector.y.vector()[cell_num],
                simulation.external_facet_nvector.z.vector()[cell_num]
            ])
    return Ps_dict


# main dictionary for storing generational data

def setup_initial_loading_pattern():
    print("\n>> converting flat load to equivalent vacuum application pattern...")
    global population
    population = {
        "parents": {},
        "children": {},
        "evolution_results": {},
        "generation_num": 1
    }
    population["parents"][1] = {}
    for phi_i in cells_with_external_facets_dict.keys():
        # write to the cells dictionary the current pressure vector norm (might be useful for transfering part of the load somewhere else later)
        cells_with_external_facets_dict[phi_i]["original_Ps_norm"] = numpy.ceil(vector_norm(average_Ps_vector(phi_i))).astype(int)
        population["parents"][1][phi_i] = cells_with_external_facets_dict[phi_i]["original_Ps_norm"]
    # ##
    if not simulation.use_vacuum:
        simulation.use_vacuum = True
        simulation.compute(Ps_dict=Ps_dict_generator(population["parents"][1]))


def limits_translation_left_part_coef(old_limits, new_limits):
    # make sure we have limits as arrays sorted from the highest value to the lowest
    old_limits = numpy.flip(numpy.sort(numpy.asarray(old_limits)), axis=0)
    new_limits = numpy.flip(numpy.sort(numpy.asarray(new_limits)), axis=0)
    return ((new_limits[0] - old_limits[1]) / (new_limits[0] - new_limits[1]))


def limits_translation_middle_part_coef(old_limits, new_limits):
    # make sure we have limits as arrays sorted from the highest value to the lowest
    old_limits = numpy.flip(numpy.sort(numpy.asarray(old_limits)), axis=0)
    new_limits = numpy.flip(numpy.sort(numpy.asarray(new_limits)), axis=0)
    return ((old_limits[0] - old_limits[1]) / (new_limits[0] - new_limits[1]))


def limits_translation_right_part_coef(old_limits, new_limits):
    # make sure we have limits as arrays sorted from the highest value to the lowest
    old_limits = numpy.flip(numpy.sort(numpy.asarray(old_limits)), axis=0)
    new_limits = numpy.flip(numpy.sort(numpy.asarray(new_limits)), axis=0)
    return ((old_limits[0] - new_limits[1]) / (new_limits[0] - new_limits[1]))


def limits_translation_pattern_coef(pattern, old_limits, new_limits):
    #print("Between", new_limits, "and", old_limits, ":")
    #print(pattern)
    if (numpy.asarray(pattern) == numpy.asarray([[0, 0], [0, 0]])).all():
        return 0
    elif (numpy.asarray(pattern) == numpy.asarray([[1, 1], [1, 1]])).all():
        return 0
    elif (numpy.asarray(pattern) == numpy.asarray([[0, 1], [0, 1]])).all():
        return 1
    elif (numpy.asarray(pattern) == numpy.asarray([[0, 1], [0, 0]])).all():
        return limits_translation_left_part_coef(old_limits=old_limits, new_limits=new_limits)
    elif (numpy.asarray(pattern) == numpy.asarray([[1, 1], [0, 0]])).all():
        return limits_translation_middle_part_coef(old_limits=old_limits, new_limits=new_limits)
    elif (numpy.asarray(pattern) == numpy.asarray([[1, 1], [0, 1]])).all():
        return limits_translation_right_part_coef(old_limits=old_limits, new_limits=new_limits)


def translate_from_old_limits(parent_num, child_num, population):
    # (re)initialize the destination dictionary
    population["children"][child_num] = {}
    # set a few useful variables
    low_limits = numpy.asarray(list(population["parents"][parent_num].keys())) - min(population["parents"][parent_num].keys())
    high_limits = numpy.asarray(list(population["parents"][parent_num].keys())) + min(population["parents"][parent_num].keys())
    new_low_limits = numpy.array(list(cells_with_external_facets_dict.keys())) - min(cells_with_external_facets_dict.keys())
    new_high_limits = numpy.array(list(cells_with_external_facets_dict.keys())) + min(cells_with_external_facets_dict.keys())
    limits = numpy.flip(numpy.union1d(high_limits, low_limits), axis=0)
    new_limits = numpy.flip(numpy.union1d(new_high_limits, new_low_limits), axis=0)
    limits_interaction_matrix = (new_limits[:, None] - limits[:] >= 0).astype(int)
    # main "translation" loop
    for new_coef_k in range(0, len(cells_with_external_facets_dict.keys())):
        new_center = list(cells_with_external_facets_dict.keys())[new_coef_k]
        new_coef_k_value = 0
        for old_coef_l in range(0, len(population["parents"][parent_num].keys())):
            new_coef_k_value += (
                limits_translation_pattern_coef(
                    pattern=(limits_interaction_matrix[new_coef_k:new_coef_k+2, old_coef_l:old_coef_l+2]),
                    old_limits=([limits[old_coef_l], limits[old_coef_l+1]]),
                    new_limits=([new_limits[new_coef_k], new_limits[new_coef_k+1]])
                ) * list(population["parents"][parent_num].values())[old_coef_l]
            )
            # end of the old_coef_l loop
        population["children"][child_num].update({
            new_center: int(numpy.round(new_coef_k_value)),
        })
        # end of the new_coef_k loop
    #print(population["children"][child_num])


def genetic_transmission(parent_num, child_num, population):
    if population["parents"][parent_num].keys() == cells_with_external_facets_dict.keys():
        population["children"][child_num] = population["parents"][parent_num].copy()
    else:
        translate_from_old_limits(parent_num=parent_num, child_num=child_num, population=population)


def genetic_mutation(child_num, population):
    _from = random.choice(numpy.asarray(list(population["children"][child_num].keys()))[numpy.asarray(list(population["children"][child_num].values())).nonzero()[0]])
    _to = random.choice(numpy.asarray(list(population["children"][child_num].keys())))
    _amount = numpy.round(random.uniform(0, 1) * population["children"][child_num][_from]).astype(int)
    #tqdm.write(f"{_from} --> {_to} : {_amount}")
    population["children"][child_num][_from] -= _amount
    # should not be needed, but better make sure
    if population["children"][child_num][_from] < 0:
        population["children"][child_num][_from] = 0
    population["children"][child_num][_to] += _amount


def get_current_internal_pressure_CoV():
    # do not take into account extreme values in the variation coefficient calculation
    sim_pressure = simulation.pressure_mmHg.vector()[:].copy()
    sim_pressure[sim_pressure < min_pressure_on_scale] = min_pressure_on_scale
    sim_pressure[sim_pressure > max_pressure_on_scale] = max_pressure_on_scale
    return (sim_pressure.std() / sim_pressure.mean())


def generate_and_compute_children(n=10, keypressed_list=[]):
    pn = numpy.asarray(list(population["parents"].keys())).max().astype(int)
    generation_better_values = tqdm(total=(pn*n), desc="better than last generation", leave=False, bar_format="{desc}:            {percentage:3.1f}%|{bar}| {n_fmt}/{total_fmt}")
    current_generation_range = tqdm(range(1, (pn*n)+1), desc="generated children", leave=False)
    for i in current_generation_range:
        # first copy the parent
        p = numpy.ceil(i/n).astype(int)
        genetic_transmission(parent_num=p, child_num=i, population=population)
        # then, add a random mutation (load transfer from one angle (phi) to another)
        # but not for the first child of each parent
        if (i%n) != 1:
            genetic_mutation(child_num=i, population=population)
        #
        if simulation.log_level < 30:
            print(colored(f">> Computing child number {i:d}...", 'yellow'))
        simulation.compute(Ps_dict=Ps_dict_generator(population["children"][i]), show_summary=False)
        # save results in the population dictionary
        population["evolution_results"][i] = get_current_internal_pressure_CoV()
        # add colors to see if the results are better of worse than the previous best (parent 1 --> child 1)
        if population["evolution_results"][i] < population["evolution_results"][1]:
            res_color = 'green'
            generation_better_values.update(1)
        elif population["evolution_results"][i] > population["evolution_results"][1]:
            res_color = 'red'
        else:
            res_color = 'white'
        tqdm.write(f"""{{ {i:d} }} : result = {colored(f"{population['evolution_results'][i]:f}", res_color)} """)
        # detect if a key has been pressed
        if len(keypressed_list) > 0:
            tqdm.write(colored("## A key has been pressed.", 'yellow'))
            if str(keypressed_list[0]).lower().find('p') >=0:
                tqdm.write(colored("## A picture will be generated at the end of this generation.", 'cyan'))
            if str(keypressed_list[0]).lower().find('q') >=0:
                tqdm.write(colored("## The process will be stopped at the end of this generation.", 'yellow'))


def select_children_for_next_generation(n=20):
    all_children = numpy.asarray(list(population["evolution_results"].items()))
    sorted_children = all_children[all_children[:, 1].argsort()]
    c_index = 1
    for child_num in sorted_children.T[0, 0:n]:
        population["parents"][c_index] = population["children"][child_num].copy()
        c_index += 1


def save_current_population_to_file(filename):
    to_file = open(filename, 'wb')
    pickle.dump(population, to_file)


def load_population_from_file(filename):
    global population
    from_file = open(filename, 'rb')
    population = pickle.load(from_file)


def input_thread(keypressed_list, _stop):
    while _stop.count(True) == 0:
        keypressed_list.append(input())
        print("(input thread running)")
    print("(stopping input thread)")
    _thread.exit_thread()


def evolution(generation_limit=args.generation_limit, picture_gen_cycle=25, resume=args.resume):
    # don't forget to make sure the mesh deformation has been undone
    if simulation.mesh_is_deformed:
        simulation.undo_mesh_deformation()
        simulation.update_exterior_facets_properties()

    # init before genetic algorithm
    coordinates_mapping()

    if not resume:
        # generate initial picture
        if not args.vacuum:
            generate_picture(f"evolution/flat_load_{int(simulation.loadweight * 1000):d}.jpg")
        else:
            generate_picture(f"evolution/vacuum_{int(simulation.loadweight * 1000):d}.jpg")

        # convert the initial flat load problem into a vacuum pressure problem
        setup_initial_loading_pattern()
        generate_picture("evolution/initial.jpg")
        print(colored(f">> Starting evolutionary process (genetic algorithm)", 'yellow'))
    else:
        # if the process has been resumed, load last known population from file
        try:
            load_population_from_file("evolution/last_known_population.obj")
        except:
            print(colored("!! An error occured while trying to load population from file. Aborting...", 'red'))

        # compute and generate an initial picture
        genetic_transmission(parent_num=1, child_num=1, population=population)
        simulation.use_vacuum = True
        simulation.compute(Ps_dict=Ps_dict_generator(population["children"][1]))
        generate_picture(str("evolution/initial_gen" + str(population["generation_num"]) + ".jpg"))
        print(colored(f">> Restarting evolutionary process (genetic algorithm)", 'yellow'))

    simulation.log_level = 32
    # detect keypressed in a different thread, to stop the process in a clean way
    global keypressed_list
    global _stop
    keypressed_list = []
    _stop = []
    _thread.start_new_thread(input_thread, (keypressed_list, _stop,))
    # keypressed_list should then be True if a key has been pressed
    # now we can start or restart the generation / selection process
    _last_picture_generated = 0
    # range (and progress bar) describing the overall progress of the algorithm
    total_generation_range = tqdm(range(int(population["generation_num"]), int(generation_limit)+1), desc="generation", leave=False)
    # we suppose that at this stage, an initial state was computed (and should be the last computation to date)
    initial_state_result = get_current_internal_pressure_CoV()
    total_improvement_over_initial_state = tqdm(total=100, desc="total improvement over initial state", unit="%", leave=False, bar_format="{desc}:   {percentage:3.1f}%|{bar}| {n_fmt}%")
    # main loop
    for generation_num in total_generation_range:
        tqdm.write(colored(f">> Generation {generation_num:d}...", 'yellow'))
        # the first generation will start with a set of (5) children
        # then we will have up to (25)
        generate_and_compute_children(keypressed_list=keypressed_list)
        # the best 5 children become parents for the next generation
        select_children_for_next_generation()
        # update the improvement meter
        total_improvement_over_initial_state.update(
            ((initial_state_result - numpy.asarray(list(population["evolution_results"])).min()) / initial_state_result) * 100
        )
        # generate a picture of the best candidate (first parent after selection) every picture_gen_cycle
        if (generation_num % picture_gen_cycle) == 0:
            tqdm.write(colored("## Generating a picture for the current generation...", 'cyan'))
            simulation.compute(Ps_dict=Ps_dict_generator(population["parents"][1]), show_summary=False)
            generate_picture(f"evolution/best_after_{generation_num:d}_generations.jpg")
            _last_picture_generated = generation_num
        # detect if a key has been pressed
        if len(keypressed_list) > 0:
            if str(keypressed_list[0]).lower().find('p') >=0:
                if _last_picture_generated < generation_num:
                    tqdm.write(colored("## Generating a picture for the current generation...", 'cyan'))
                    simulation.compute(Ps_dict=Ps_dict_generator(population["parents"][1]), show_summary=False)
                    generate_picture(f"evolution/best_after_{generation_num:d}_generations.jpg")
                    _last_picture_generated = generation_num
            if str(keypressed_list[0]).lower().find('q') >=0:
                print(colored("## A key has been pressed, stopping the process...", 'yellow'))
                _stop.append(True)
            keypressed_list.clear()
        # indicate in the dictionary that the process of this generation is over
        population["generation_num"] = generation_num + 1
        # save the last known population state at the end of each generation
        try:
            save_current_population_to_file("evolution/last_known_population.obj")
        except:
            print(colored("!! An error occured while trying to save the population to file. Aborting...", 'red'))
        # check if the stop signal has been sent
        if _stop.count(True) > 0:
            _stop.clear()
            break

    # ##
    # That's all...


# Run the main loop :
evolution(generation_limit=args.generation_limit)

# generate a final picture
simulation.compute(Ps_dict=Ps_dict_generator(population["parents"][1]))
generate_picture(str("evolution/final_gen" + str(population["generation_num"] - 1) + ".jpg"))

_stop.append(True)
