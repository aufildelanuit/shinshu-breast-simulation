"""
Inspired from FEniCS tutorial demo program: Linear elastic problem.

Main equation for the linear elastic problem:
  -div(sigma(u)) = f

Adapted to the needs of a research topic on breast deformation.
This program was made especially for finding optimal material properties
(Young Modulus and Poisson Ratio) for the model to conform to a certain
value of vertical displacement (observed in experiments) under its own weight.
OPOLKA Yohann, Shinshu University, Koseki Lab., 2020.04.21
Latest revision: 2020.05.13
"""

# The idea :
# This time, we are looking for the material properties, while taking into
# into account a possible offest for the applied load (because in a real
# situation, the load would often be on top of a platform, the weight of
# which should maybe not be neglected when dealing with soft materials...)
# Note that this offset could also be an arbitrary way to compensate for
# a difference in rigidity under load between the model and the simulation...
#
# The process :
# First, material properties are estimated in a "no-load" (+ offset) condition,
# then, the vertical displacement under load (0.200, 0.400 and 0.600 kg) is
# calculated, and compared to expected (experimental) values (squared sum).
# A search for the minimum of the squared sum might be time-consuming, but
# should yield an estimate of the offset allowing the best fitting of the
# displacement vs load curve.


from simulation import New_Simulation
import numpy
import argparse

# Initialize the argument parser with a description of what the program does
desc = 'Tries to find optimal materials parameters for the silicone simulation'
parser = argparse.ArgumentParser(description=desc)
# Set possible arguments and their default values
# Note that nargs='?' means 0 or 1 argument of this type can be specified
# const is the value taken when the argument name is given alone
# default is the value taken when the argument is not specified at all
parser.add_argument(
    '--loadweight-offset-lower-bound', '--lolb',
    type=float,
    nargs='?',
    help='lower limit for the loadweight offset optimization, in kg',
    const=0,
    default=0
)
parser.add_argument(
    '--loadweight-offset-upper-bound', '--loub',
    type=float,
    nargs='?',
    help='upper limit for the loadweight offset optimization, in kg',
    const=0.200,
    default=0.200
)
parser.add_argument(
    '--loadweight-offset-value', '--lov',
    type=float,
    nargs='?',
    help='set the value of the loadweight offset, in kg, instead of using optimization',
    const=0,
    default=numpy.NaN
)
parser.add_argument(
    '--max-iterations',
    type=int, nargs='?',
    help='max number of iterations for the optimization loop(s)',
    const=50,
    default=50
)
parser.add_argument(
    '-m', '--mesh-file',
    type=str, nargs='?',
    help='specify a mesh file to use for the simulation',
    const=None,
    default=None
)
parser.add_argument(
    '--optimize-young-modulus-search', '--oyms',
    type=bool,
    nargs='?',
    help='use optimization for the young modulus search',
    const=True,
    default=False
)
parser.add_argument(
    '--optimize-loadweight-offset-search', '--olos',
    type=bool,
    nargs='?',
    help='use optimization for the loadweight offset search',
    const=True,
    default=False
)
parser.add_argument(
    '--poisson-ratio',
    type=float,
    nargs='?',
    help='set the poisson ratio to a constant value',
    default=0.47
)
parser.add_argument(
    '-q', '--quarter-mesh',
    type=bool,
    nargs='?',
    help='use only a quarter of an hemisphere for the simulation',
    const=True,
    default=False
)
parser.add_argument(
    '-v', '--log-level',
    type=int,
    nargs='?',
    help='int value: DBG=10, TRACE=13, PROGRESS=16, INFO=20, WARNING=30, ERROR=40, CRITICAL=50',
    const=16,
    default=30
)
parser.add_argument(
    '--young-lower-bound', '--ylb',
    type=float,
    nargs='?',
    help='lower limit for the young modulus optimization, in Pa',
    const=1,
    default=1
)
parser.add_argument(
    '--young-upper-bound', '--yub',
    type=float,
    nargs='?',
    help='upper limit for the young modulus optimization, in Pa',
    const=4000,
    default=4000
)

args = parser.parse_args()

loadweight_offset_lower_bound = args.loadweight_offset_lower_bound
loadweight_offset_upper_bound = args.loadweight_offset_upper_bound
loadweight_offset_value = args.loadweight_offset_value
max_iterations = args.max_iterations
mesh_file = args.mesh_file
optimize_young_modulus_search = args.optimize_young_modulus_search
optimize_loadweight_offset_search = args.optimize_loadweight_offset_search
poisson_r = args.poisson_ratio
quarter_mesh = args.quarter_mesh
log_level = args.log_level
young_lower_bound = args.young_lower_bound
young_upper_bound = args.young_upper_bound

# ratio observed between the Young modulus of the skin and adipose
skin_adipose_E_ratio = 367.60/8

# expected vertical displacement for load conditions 0, 0.200, 0.400 and 0.600 kg
experimental_load_conditions = [0, 0.200, 0.400, 0.600]
expected_vertical_displacement = [-0.007, -0.016, -0.020, -0.022]

# precision of the experimental measurements (mm = 0.001 m)
precision = 0.001    # 0.001
# criterion for the stability test in the optimization search by dichotomy
search_tolerance_loadweight_offset = 0.0001   # in kg
search_tolerance_young_modulus = 1  # in Pa
stability_criterion = 0.0001  # no unit, relative


# a function used for searching an optimum (min or max) by 3-point dichotomy
# uses left or right guesses to optimize the research time
def search_by_dichotomy(
    function,
    left_limit,
    right_limit,
    max_iterations=50,
    data_dictionary=None,
    search_tolerance=0.00001,
    stability_criterion=stability_criterion,
    log_level=25,
    log_level_print_limit=35,
    search_for="min"
):
    next_step = 'L'
    stop = False
    ms_iter = 0
    progress = 0
    # check that the left and right limit are not inverted
    if right_limit < left_limit:
        temp_var = right_limit
        right_limit = left_limit
        left_limit = temp_var
    if data_dictionary is not None:
        data_dictionary.update({
            "current_left_limit": left_limit,
            "current_right_limit": right_limit
        })
    while True:
        ms_iter += 1
        # preparation
        mid = (left_limit + right_limit)/2
        if next_step == 'R':
            quarter = (mid + right_limit)/2
        elif next_step == 'L':
            quarter = (left_limit + mid)/2
        # actual calculation
        if log_level < log_level_print_limit:
            print(f"\r>># Searching optimum within [{left_limit:f}, {right_limit:f}], iteration {ms_iter:d}  \t~ {int(numpy.round(progress)):d}%  \t", end='')
        f_quarter = function(quarter)
        # save data
        if data_dictionary is not None:
            if quarter not in data_dictionary:
                data_dictionary.update({quarter: {}})
            data_dictionary[quarter].update({
                "output_f": f_quarter
            })
        f_mid = function(mid)
        # save data
        if data_dictionary is not None:
            if mid not in data_dictionary:
                data_dictionary.update({mid: {}})
            data_dictionary[mid].update({
                "output_f": f_mid
            })
        if f_mid == 0:
            f_mid_divsafe = stability_criterion
        else:
            f_mid_divsafe = f_mid
        # progress
        progress = numpy.log10(numpy.abs((f_mid - f_quarter)/f_mid_divsafe))/(numpy.log10(stability_criterion))*100
        if progress < 0:
            progress = 0
        if progress > 100:
            progress = 100
        # test : optimum found? search search_tolerance
        if numpy.abs(right_limit - left_limit) < search_tolerance:
            stop = True
        # test : optimum found? stability_criterion
        if (numpy.abs((f_mid - f_quarter)/f_mid_divsafe) < stability_criterion) and (stop == False):
            f_check = function((quarter + mid)/2)
            # save data
            if data_dictionary is not None:
                if quarter not in data_dictionary:
                    data_dictionary.update({((quarter + mid)/2): {}})
                data_dictionary[((quarter + mid)/2)].update({
                    "output_f": f_check
                })
            if numpy.abs((f_mid - f_check)/f_mid_divsafe) < stability_criterion:
                stop = True
        # preparing for the next iteration
        if not stop:
            current_step = next_step
            if search_for == "min":
                if f_quarter >= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = quarter
                        next_step = 'R'
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = quarter
                        next_step = 'L'
                if f_quarter <= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = mid
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = mid
            elif search_for == "max":
                if f_quarter <= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = quarter
                        next_step = 'R'
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = quarter
                        next_step = 'L'
                if f_quarter >= f_mid:
                    if current_step == 'L':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_right_limit": right_limit})
                        right_limit = mid
                    elif current_step == 'R':
                        if data_dictionary is not None:
                            data_dictionary.update({"current_left_limit": left_limit})
                        left_limit = mid
        # feedback
        if log_level < (log_level_print_limit-10):
            print(f"f = {f_mid:f}, next:{next_step:s}, stop:{stop:b}")
        # loop break conditions
        if ms_iter >= max_iterations:
            break
        if stop:
            break
        # End of the search loop
    if log_level < log_level_print_limit:
        print("")
    return (mid, f_mid, ms_iter)


def sum_of_squared_diff(a_array, b_array, precision=1):
    return (((numpy.asarray(a_array)*(1/precision) - numpy.asarray(b_array)*(1/precision))**2).sum())


class Data():
    pass


class New_Simulation(New_Simulation):

    def __init__(
        self,
        loadweight=0.000,
        loadweight_offset=0.000,
        quarter_mesh=True,
        log_level=30,
        plot_level=0,
        no_init=False,
        mesh_file=None
    ):
        super().__init__(
            loadweight=loadweight,
            loadweight_offset=loadweight_offset,
            quarter_mesh=quarter_mesh,
            log_level=log_level,
            plot_level=plot_level,
            no_init=no_init,
            mesh_file=mesh_file
        )
        self.data = Data()
        self.data.young_left_limit = young_lower_bound
        self.data.young_right_limit = young_upper_bound
        self.data.z_criterion_left_limit = {
            0.0: self.load_balance_z_criterion_min
        }
        self.data.z_criterion_right_limit = {
            0.0: self.load_balance_z_criterion_max
        }
        self.data.z_criterion_left_limit_by_offset = {
            0.0: self.load_balance_z_criterion_min
        }
        self.data.z_criterion_right_limit_by_offset = {
            0.0: self.load_balance_z_criterion_max
        }
        self.data.original_z_criterion_min = self.load_balance_z_criterion_min
        self.data.original_z_criterion_max = self.load_balance_z_criterion_max
        self.data.by_loadweight_offset = {}
        self.data.by_young_modulus = {}

    ##
    def squared_diff_test_young_modulus(
        self,
        young_modulus,
        poisson_ratio=poisson_r,
        skin_adipose_E_ratio=skin_adipose_E_ratio,
        load_conditions=experimental_load_conditions,
        precision=precision,
        optimize=optimize_young_modulus_search
    ):
        # avoid doing the test again if it has already been done in the same conditions
        if young_modulus not in self.data.by_young_modulus:
            # preparation
            self.data.by_young_modulus.update({
                young_modulus: {"z_criterion_by_load": {}}
            })
            young_modulus_current_left_limit = self.data.by_young_modulus["current_left_limit"]
            young_modulus_current_right_limit = self.data.by_young_modulus["current_right_limit"]
            if (young_modulus_current_left_limit in self.data.by_young_modulus) and optimize:
                for load_i in self.data.by_young_modulus[young_modulus_current_left_limit]["z_criterion_by_load"].keys():
                    self.data.z_criterion_right_limit[load_i] = self.data.by_young_modulus[young_modulus_current_left_limit]["z_criterion_by_load"][load_i]
            else:
                for load_i in experimental_load_conditions:
                    self.data.z_criterion_right_limit[load_i] = self.data.z_criterion_right_limit_by_offset[load_i]
            if (young_modulus_current_right_limit in self.data.by_young_modulus) and optimize:
                for load_i in self.data.by_young_modulus[young_modulus_current_right_limit]["z_criterion_by_load"].keys():
                    self.data.z_criterion_left_limit[load_i] = self.data.by_young_modulus[young_modulus_current_right_limit]["z_criterion_by_load"][load_i]
            else:
                for load_i in experimental_load_conditions:
                    self.data.z_criterion_left_limit[load_i] = self.data.z_criterion_left_limit_by_offset[load_i]

            if self.log_level < 30:
                print("\n############")
            self.set_material_properties(
                young_modulus_values=[skin_adipose_E_ratio*young_modulus, young_modulus, young_modulus],   # in Pa
                poisson_ratio_values=[0.49, poisson_ratio, poisson_ratio]
            )
            self.assign_material_properties_to_cells()
            # End of material properties definition

            self.estimated_vertical_displatement = []
            for load_i in load_conditions:
                # before the calculation
                self.update_loadweight(new_loadweight=load_i)
                # note : updating the loadweight resets the z_criterion limits
                if (load_i in self.data.z_criterion_left_limit) and not (numpy.isnan(self.data.z_criterion_left_limit[load_i])):
                    if (self.data.z_criterion_left_limit[load_i] > self.load_balance_z_criterion) or (self.loadweight == load_conditions[0]):
                        self.load_balance_z_criterion_min = self.data.z_criterion_left_limit[load_i]
                    else:
                        self.load_balance_z_criterion_min = self.load_balance_z_criterion
                else:
                    self.load_balance_z_criterion_min = self.load_balance_z_criterion
                if (load_i in self.data.z_criterion_right_limit) and not (numpy.isnan(self.data.z_criterion_right_limit[load_i])):
                    self.load_balance_z_criterion_max = self.data.z_criterion_right_limit[load_i]
                else:
                    self.load_balance_z_criterion_max = self.data.original_z_criterion_max
                # calculation
                self.compute(save_to_files=False, keep_mesh_deformation=False, show_summary=False)
                # after the calculation
                self.data.by_young_modulus[young_modulus]["z_criterion_by_load"].update({
                    load_i: self.load_balance_z_criterion
                })
                self.estimated_vertical_displatement.append(self.top_point_displacement)

            return sum_of_squared_diff(expected_vertical_displacement, self.estimated_vertical_displatement, precision=precision)

        else:
            return self.data.by_young_modulus[young_modulus]["output_f"]

    ##
    def squared_diff_test_loadweight_offset(self, loadweight_offset, precision=precision, optimize=optimize_loadweight_offset_search):
        if loadweight_offset not in self.data.by_loadweight_offset:

            # preparation
            self.data.by_loadweight_offset.update({
                loadweight_offset: {"optimal_young_modulus": numpy.NaN}
            })
            # before the calculation and optimization
            self.update_loadweight(new_loadweight_offset=loadweight_offset)
            # empty the young_modulus optimization data dictionary
            self.data.by_young_modulus = {}

            # for the optimization of the "research window"
            loadweight_offset_left_limit = self.data.by_loadweight_offset["current_left_limit"]
            loadweight_offset_right_limit = self.data.by_loadweight_offset["current_right_limit"]
            if (loadweight_offset_left_limit in self.data.by_loadweight_offset) and optimize:
                self.data.young_left_limit = self.data.by_loadweight_offset[loadweight_offset_left_limit]["optimal_young_modulus"]
                for load_i in self.data.by_loadweight_offset[loadweight_offset_left_limit]["z_criterion_by_load"].keys():
                    self.data.z_criterion_right_limit_by_offset[load_i] = self.data.by_loadweight_offset[loadweight_offset_left_limit]["z_criterion_by_load"][load_i]
            else:
                self.data.young_left_limit = young_lower_bound
                for load_i in experimental_load_conditions:
                    self.data.z_criterion_right_limit_by_offset[load_i] = self.data.original_z_criterion_max
            if (loadweight_offset_right_limit in self.data.by_loadweight_offset) and optimize:
                self.data.young_right_limit = self.data.by_loadweight_offset[loadweight_offset_right_limit]["optimal_young_modulus"]
                for load_i in self.data.by_loadweight_offset[loadweight_offset_right_limit]["z_criterion_by_load"].keys():
                    self.data.z_criterion_left_limit_by_offset[load_i] = self.data.by_loadweight_offset[loadweight_offset_right_limit]["z_criterion_by_load"][load_i]
            else:
                self.data.young_right_limit = young_upper_bound
                for load_i in experimental_load_conditions:
                    self.data.z_criterion_left_limit_by_offset[load_i] = self.data.original_z_criterion_min

            optimal_young_modulus, squared_diff, iteration_num = search_by_dichotomy(
                function=simulation.squared_diff_test_young_modulus,
                left_limit=self.data.young_left_limit,
                right_limit=self.data.young_right_limit,
                max_iterations=max_iterations,
                data_dictionary=simulation.data.by_young_modulus,
                search_tolerance=search_tolerance_young_modulus,
                search_for="min",
                log_level=simulation.log_level,
                log_level_print_limit=32
            )
            self.data.by_loadweight_offset[loadweight_offset].update({
                "optimal_young_modulus": optimal_young_modulus,
                "z_criterion_by_load": self.data.by_young_modulus[optimal_young_modulus]["z_criterion_by_load"]
            })

            R_squared = ((numpy.corrcoef(self.estimated_vertical_displatement, expected_vertical_displacement)[0, 1])**2)

            if self.log_level < 32:
                print("########################################################")
                print(f"Optimized problem data (ν = {simulation.poisson_ratio_values[1]:f}) after {iteration_num:d} iteration(s): ")
                print(f'Skin:\n\tρ = {self.rho_values[0]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[0]:f} Pa\n\tν = {self.poisson_ratio_values[0]:f} (ratio)\n\tλ = {self.lambda__values[0]:f} Pa\n\tμ = {self.mu_values[0]:f} Pa')
                print(f'Adipose tissue:\n\tρ = {self.rho_values[1]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[1]:f} Pa\n\tν = {self.poisson_ratio_values[1]:f} (ratio)\n\tλ = {self.lambda__values[1]:f} Pa\n\tμ = {self.mu_values[1]:f} Pa')
                print(f'Glandular tissue:\n\tρ = {self.rho_values[2]:f} kg・m^(-3)\n\tE = {self.young_modulus_values[2]:f} Pa\n\tν = {self.poisson_ratio_values[2]:f} (ratio)\n\tλ = {self.lambda__values[2]:f} Pa\n\tμ = {self.mu_values[2]:f} Pa')
                print(f'Gravity constant:\n\tg = {self.g:f} m・s^(-2)')
                print(f'Loadweight offset:\n\tm = {self.loadweight_offset:f} kg')
                print(f'Correlation:\n\tR^2 = {R_squared:f}\n\tSquared diff = {squared_diff:f}')
                print("Displacement:")
                for info in [experimental_load_conditions, expected_vertical_displacement, simulation.estimated_vertical_displatement]:
                    if info == experimental_load_conditions:
                        info_unit = "kg"
                    else:
                        info_unit = "m"
                    for i in range(0, len(info)):
                        print(f"\t{info[i]:f} {info_unit:s}\t", end='')
                    print('')
                print("########################################################\n")
                # Coordinates of the point with u_max as a displacement value : mesh.coordinates()[numpy.where(u_magnitude.vector()[:] == u_max)[0][0]]
                # Displacement of the top point : u_magnitude.vector()[numpy.where(mesh.coordinates()[:, 2] == 0.07)[0][0]]

            # returns the square of the scaled difference between expected and estimated values
            # this function is to be minimized (least squares method)
            return squared_diff

        # in the case the same parameters have already been calculated
        else:
            return self.data.by_loadweight_offset[loadweight_offset]["output_f"]


# Initializing a simulation from the (slightly modified) New_Simulation object
simulation = New_Simulation(
    loadweight=0.000,
    quarter_mesh=args.quarter_mesh,
    log_level=log_level,
    plot_level=0,
    no_init=False,
    mesh_file=mesh_file
)

# Initialize material properties
simulation.set_material_properties(
    rho_values=[1092, 970, 970],
    young_modulus_values=[367.6*(10**3), young_upper_bound, young_upper_bound],
    poisson_ratio_values=[0.49, poisson_r, poisson_r]
)

# initialize material properties as fenics functions
# functions can be adressed as (for example:) simulation.young_modulus.vector()[:]
simulation.assign_material_properties_to_cells()


if numpy.isnan(loadweight_offset_value):
    # Main optimization loop
    optimal_loadweight_offset, squared_diff, iteration_num = search_by_dichotomy(
        function=simulation.squared_diff_test_loadweight_offset,
        left_limit=loadweight_offset_lower_bound,
        right_limit=loadweight_offset_upper_bound,
        max_iterations=max_iterations,
        data_dictionary=simulation.data.by_loadweight_offset,
        search_tolerance=search_tolerance_loadweight_offset,
        search_for="min",
        log_level=simulation.log_level,
        log_level_print_limit=35
    )
else:
    simulation.data.by_loadweight_offset.update({
        "current_left_limit": loadweight_offset_value,
        "current_right_limit": loadweight_offset_value
    })
    iteration_num = 1
    optimal_loadweight_offset = loadweight_offset_value
    squared_diff = simulation.squared_diff_test_loadweight_offset(loadweight_offset_value)


# squared coefficient of correlation between estimated and expected values
R_squared = ((numpy.corrcoef(simulation.estimated_vertical_displatement, expected_vertical_displacement)[0, 1])**2)

print("\n\n\n########################################################")
print(f"Final results (ν = {simulation.poisson_ratio_values[1]:f}) after {iteration_num:d} iteration(s): ")
print(f'Skin:\n\tρ = {simulation.rho_values[0]:f} kg・m^(-3)\n\tE = {simulation.young_modulus_values[0]:f} Pa\n\tν = {simulation.poisson_ratio_values[0]:f} (ratio)\n\tλ = {simulation.lambda__values[0]:f} Pa\n\tμ = {simulation.mu_values[0]:f} Pa')
print(f'Adipose tissue:\n\tρ = {simulation.rho_values[1]:f} kg・m^(-3)\n\tE = {simulation.young_modulus_values[1]:f} Pa\n\tν = {simulation.poisson_ratio_values[1]:f} (ratio)\n\tλ = {simulation.lambda__values[1]:f} Pa\n\tμ = {simulation.mu_values[1]:f} Pa')
print(f'Glandular tissue:\n\tρ = {simulation.rho_values[2]:f} kg・m^(-3)\n\tE = {simulation.young_modulus_values[2]:f} Pa\n\tν = {simulation.poisson_ratio_values[2]:f} (ratio)\n\tλ = {simulation.lambda__values[2]:f} Pa\n\tμ = {simulation.mu_values[2]:f} Pa')
print(f'Gravity constant:\n\tg = {simulation.g:f} m・s^(-2)')
print(f'Loadweight offset:\n\tm = {optimal_loadweight_offset:f} kg')
print(f'Correlation:\n\tR^2 = {R_squared:f}\n\tSquared diff = {squared_diff:f}')
print("Displacement:")
for info in [experimental_load_conditions, expected_vertical_displacement, simulation.estimated_vertical_displatement]:
    if info == experimental_load_conditions:
        info_unit = "kg"
    else:
        info_unit = "m"
    for i in range(0, len(info)):
        print(f"\t{info[i]:f} {info_unit:s}\t", end='')
    print('')
print("########################################################\n")
